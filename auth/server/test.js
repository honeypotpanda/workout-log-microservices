'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const bcrypt = require('bcrypt');
const should = chai.should();
const server = require('./server');
const User = require('./user');
const jwt = require('jsonwebtoken');
const generateHash = password => bcrypt.hashSync(password, 1);
const generateJwt = (id, expiry, secret) => (
  jwt.sign({
    iss: 'workout-log-microservices',
    sub: id
  }, secret, { expiresIn: expiry })
);

chai.use(chaiHttp);

describe('Auth', () => {

  before(done => {
    User
      .sync({ force: true })
      .then(() => User.create({ email: 'test1@test.com', password: generateHash('password') }))
      .then(() => done())
      .catch(done);
  });

  after(done => {
    User
      .drop()
      .then(() => done())
      .catch(done);
  });

  describe('GET /auth/token', () => {

    it('should 200 for a valid token', done => {
      const validToken = generateJwt(1, '2h', process.env.JWT_SECRET);
      chai
        .request(server)
        .get('/auth/token')
        .set('Authorization', `Bearer ${validToken}`)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(200);
          done();
        });
    });

    it('should 401 for an invalid token', done => {
      const invalidToken = generateJwt(1, '2h', 'invalid-secret');
      chai
        .request(server)
        .get('/auth/token')
        .set('Authorization', `Bearer ${invalidToken}`)
        .end((err, res) => {
          should.exist(err);
          res.should.have.status(401);
          done();
        });
    });

    it('should 401 for no token', done => {
      chai
        .request(server)
        .get('/auth/token')
        .end((err, res) => {
          should.exist(err);
          res.should.have.status(401);
          done();
        });
    });

  });

  describe('POST /auth/signup', () => {

    it('should POST a user', done => {
      chai
        .request(server)
        .post('/auth/signup')
        .send({ email: 'test2@test.com', password: 'password' })
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(200);
          should.exist(res.body.token);
          done();
        });
    });

    it('should not POST a user with an invalid email', done => {
      const user = { email: 'test', password: 'password' };
      chai
        .request(server)
        .post('/auth/signup')
        .send(user)
        .end((err, res) => {
          res.should.have.status(409);
          done();
        });
    });

    it('should not POST a user without an email', done => {
      chai
        .request(server)
        .post('/auth/signup')
        .send({ password: 'password' })
        .end((err, res) => {
          res.should.have.status(206);
          done();
        });
    });

    it('should not POST a user without a password', done => {
      chai
        .request(server)
        .post('/auth/signup')
        .send({ email: 'test3@test.com' })
        .end((err, res) => {
          res.should.have.status(206);
          done();
        });
    });

    it('should not POST a user with a non-unique email', done => {
      chai
        .request(server)
        .post('/auth/signup')
        .send({ email: 'test1@test.com', password: 'password' })
        .end((err, res) => {
          res.should.have.status(409);
          done();
        });
    });

  });

  describe('POST /auth/signin', () => {

    it('should sign in user', done => {
      chai
        .request(server)
        .post('/auth/signin')
        .send({ email: 'test1@test.com', password: 'password' })
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(200);
          should.exist(res.body.token);
          done();
        });
    });

    it('should not sign in user without an email', done => {
      chai
        .request(server)
        .post('/auth/signin')
        .send({ password: 'password' })
        .end((err, res) => {
          res.should.have.status(206);
          done();
        });
    });

    it('should not sign in user without a password', done => {
      chai
        .request(server)
        .post('/auth/signin')
        .send({ email: 'test1@test.com' })
        .end((err, res) => {
          res.should.have.status(206);
          done();
        });
    });

    it('should not sign in user with an unregistered email', done => {
      chai
        .request(server)
        .post('/auth/signin')
        .send({ email: 'unregistered-email@test.com', password: 'password' })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should not sign in user with valid email and incorrect password', done => {
      chai
        .request(server)
        .post('/auth/signin')
        .send({ email: 'test1@test.com', password: 'invalid-password' })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

  });

});