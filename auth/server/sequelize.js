'use strict';

const Sequelize = require('sequelize');
let database = process.env.DB_DATABASE;
if (process.env.NODE_ENV !== 'production') {
  database = `${database}_${process.env.NODE_ENV}`;
}
const host = process.env.NODE_ENV === 'production' ? 'db' : process.env.DB_HOST;
const sequelize = new Sequelize(database, process.env.DB_USER, process.env.DB_PASSWORD, {
  host: host,
  dialect: 'mysql',
  logging: false,
  timezone: 'America/Los_Angeles'
});

module.exports = sequelize;