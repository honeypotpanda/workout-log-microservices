'use strict';

require('dotenv').config();
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const cors = require('cors');
const User = require('./user');
const app = express();
const validateToken = expressJwt({ secret: process.env.JWT_SECRET });
const generateJwt = (id, expiry) => (
  jwt.sign({
    iss: 'workout-log-microservices',
    sub: id
  }, process.env.JWT_SECRET, { expiresIn: expiry })
);
const SALT_ROUNDS = process.env.NODE_ENV !== 'production' ? 1 : 10;

let port;
if (process.env.NODE_ENV === 'production') {
  port = process.env.PORT;
} else if (process.env.NODE_ENV === 'development') {
  port = 3001;
} else {
  port = 3002;
}

if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
  User
    .sync()
    .catch(console.error);
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  origin: ['http://localhost:5000'],
  credentials: true
}));

app.get('/auth/token', validateToken, (req, res) => {
  res.sendStatus(200);
});

app.post('/auth/signup', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(206).send({ message: 'Email and password are required.' });
  }

  bcrypt.hash(password, SALT_ROUNDS, (err, hash) => {
    if (err) {
      return res.status(500).send(err);
    }

    User
      .create({ email: email, password: hash })
      .then(user => {
        const token = generateJwt(user.get('id'), '2h');
        res.status(200).send({ token: token });
      })
      .catch(err => {
        const error = err.errors[0];
        if (error.type === 'unique violation' && error.path === 'email') {
          return res.status(409).send({ message: 'Email has already been registered.' });
        }
        res.status(409).send(error);
      });
  });
});

app.post('/auth/signin', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(206).send({ message: 'Email and password are required.' });
  }

  User
    .findOne({
      where: {
        email: email
      }
    })
    .then(user => {
      bcrypt.compare(password, user.get('password'), (err, match) => {
        if (err) {
          return res.status(500).send(err);
        }

        if (match) {
          const token = generateJwt(user.get('id'), '2h');
          res.status(200).send({ token: token });
        } else {
          res.status(400).send({ message: 'Your email or password was incorrect. Please try again.' });
        }
      });
    })
    .catch(() => {
      res.status(400).send({ message: 'Your email or password was incorrect. Please try again.' });
    });
});

app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
  next();
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(port, () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`Server is listening on port ${port}.`);
  }
});

module.exports = app;