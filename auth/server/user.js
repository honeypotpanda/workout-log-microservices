'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./sequelize');
const User = sequelize.define('user', {
  email: {
    type: Sequelize.CHAR(30),
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  password: {
    type: Sequelize.CHAR(60),
    allowNull: false
  }
}, {
  getterMethods: {
    info: function() {
      return {
        id: this.id,
        email: this.email,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
      };
    }
  }
});

module.exports = User;