import { Map, List } from 'immutable'

import { ADD_DATE, FETCH_DATES_REQUEST, FETCH_DATES_SUCCESS,
  FETCH_DATES_FAILURE, SIGN_OUT, SET_DATE_ERROR, RESET_DATE_ERROR,
  SET_DATE_ERROR_TIMER, RESET_DATE_ERROR_TIMER } from '../constants/ActionTypes'

const initialState = Map({
  isFetching: false,
  list: List(),
  shouldResetErrorTimer: false,
  error: ''
})

const dates = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DATE:
      return state.updateIn(['list'], list => list.push(action.date))
    case FETCH_DATES_REQUEST:
      return state.merge(Map({ isFetching: true, error: '' }))
    case FETCH_DATES_SUCCESS:
      return state.merge(Map({ isFetching: false, list: List(action.dates.map(date => date.date)) }))
    case FETCH_DATES_FAILURE:
      return state.merge(Map({ isFetching: false, error: action.error.message }))
    case SIGN_OUT:
      return initialState
    case SET_DATE_ERROR:
      return state.merge(Map({ error: action.error }))
    case RESET_DATE_ERROR:
      return state.merge(Map({ error: '' }))
    case SET_DATE_ERROR_TIMER:
      return state.merge(Map({ shouldResetErrorTimer: true }))
    case RESET_DATE_ERROR_TIMER:
      return state.merge(Map({ shouldResetErrorTimer: false }))
    default:
      return state
  }
}

export default dates