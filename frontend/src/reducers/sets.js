import { Map, List } from 'immutable'

import { FETCH_SETS_REQUEST, FETCH_SETS_SUCCESS, FETCH_SETS_FAILURE,
  POST_SET_REQUEST, POST_SET_SUCCESS, POST_SET_FAILURE, DELETE_SET_REQUEST,
  DELETE_SET_SUCCESS, DELETE_SET_FAILURE, PUT_SET_REQUEST, PUT_SET_SUCCESS,
  PUT_SET_FAILURE, RESET_SETS_CHANGED, SIGN_OUT } from '../constants/ActionTypes'

const initialState = Map({
  isFetching: false,
  listByDates: Map(),
  error: '',
  setsChanged: false
})

const sets = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SETS_REQUEST:
      return state.merge(Map({ isFetching: true, error: '' }))
    case FETCH_SETS_SUCCESS:
      return state.merge(
        Map({
          isFetching: false,
          listByDates: state.get('listByDates')
                            .set(action.date,
                                 List(action.sets.map(set => Map(set))))
        })
      )
    case FETCH_SETS_FAILURE:
      return state.merge(Map({ isFetching: false, error: action.error.message }))
    case POST_SET_REQUEST:
      return state.merge(Map({ isFetching: true, error: '' }))
    case POST_SET_SUCCESS:
      return state.set('isFetching', false)
                  .set('setsChanged', true)
                  .updateIn(['listByDates', action.date],
                            sets => sets.push(Map(action.set)))
    case POST_SET_FAILURE:
      return state.merge(Map({ isFetching: false, error: action.error.message }))
    case DELETE_SET_REQUEST:
      return state.merge(Map({ isFetching: true, error: '' }))
    case DELETE_SET_SUCCESS:
      return state.set('isFetching', false)
                  .set('setsChanged', true)
                  .updateIn(['listByDates', action.date],
                            sets => sets.delete(action.index))
    case DELETE_SET_FAILURE:
      return state.merge(Map({ isFetching: false, error: action.error.message }))
    case PUT_SET_REQUEST:
      return state.merge(Map({ isFetching: true, error: '' }))
    case PUT_SET_SUCCESS:
      return state.set('isFetching', false)
                  .set('setsChanged', true)
                  .updateIn(['listByDates', action.date],
                             sets => sets.update(action.index, set => Map({
                               id: set.get('id'),
                               exercise: action.exercise,
                               weight: action.weight,
                               repetitions: action.repetitions
                             })))
    case PUT_SET_FAILURE:
      return state.merge(Map({ isFetching: false, error: action.error.message }))
    case RESET_SETS_CHANGED:
      return state.merge(Map({ setsChanged: false }))
    case SIGN_OUT:
      return initialState
    default:
      return state
  }
}

export default sets