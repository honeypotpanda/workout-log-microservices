const initialState = ''

const activeDate = (state = initialState, action) => {
  switch (action.type) {
    case 'SELECT_DATE':
      return action.date
    case 'SIGN_OUT':
      return initialState
    default:
      return state
  }
}

export default activeDate