import { combineReducers } from 'redux-immutable'
import activeDate from './activeDate'
import auth from './auth'
import dates from './dates'
import sets from './sets'

const rootReducer = combineReducers({
  auth,
  dates,
  activeDate,
  sets
})

export default rootReducer