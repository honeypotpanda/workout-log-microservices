import { Map } from 'immutable'

import { FETCH_AUTH_REQUEST, FETCH_AUTH_SUCCESS, FETCH_AUTH_FAILURE,
  SIGN_OUT, RESET_ERROR } from '../constants/ActionTypes'

const initialState = Map({
  hasFetched: false,
  isFetching: false,
  isLoggedIn: false,
  error: ''
})

const auth = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_AUTH_REQUEST:
      return state.merge(Map({ isFetching: true, error: '' }))
    case FETCH_AUTH_SUCCESS:
      return state.merge(Map({ hasFetched: true, isFetching: false, isLoggedIn: true }))
    case FETCH_AUTH_FAILURE:
      return state.merge(Map({ hasFetched: true, isFetching: false, error: action.error.message }))
    case RESET_ERROR:
      return state.merge(Map({ error: '' }))
    case SIGN_OUT:
      return initialState
    default:
      return state
  }
}

export default auth