import React, { Component, PropTypes } from 'react'
import { Button, Confirm } from 'semantic-ui-react'

class DeleteModal extends Component {
  state = { open: false }

  show = () => this.setState({ open: true })
  handleConfirm = () => {
    this.props.onDeleteClick()
    this.setState({ open: false })
  }
  handleCancel = () => this.setState({ open: false })

  render() {
    return (
      <span>
        <Button onClick={this.show}>Delete</Button>
        <Confirm
          header='Delete Set'
          open={this.state.open}
          onCancel={this.handleCancel}
          onConfirm={this.handleConfirm}
        />
      </span>
    )
  }
}

DeleteModal.propTypes = {
  onDeleteClick: PropTypes.func.isRequired
}

export default DeleteModal