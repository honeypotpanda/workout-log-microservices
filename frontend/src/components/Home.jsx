import React from 'react'
import { Link } from 'react-router'
import { Container, Header } from 'semantic-ui-react'

import NavBar from '../containers/NavBar'

const Home = () => (
  <div>
    <Container textAlign="center">
      <NavBar />
      <Header as="h1">Welcome to Workout Log!</Header>
    </Container>
  </div>
)

export default Home