import React, { Component, PropTypes } from 'react'
import { Button, Modal, Form } from 'semantic-ui-react'

class EditModal extends Component {
  state = {
    formData: {},
    modalOpen: false
  }

  handleOpen = (e) => this.setState({ modalOpen: true })

  handleClose = (e) => {
    e.preventDefault()
    this.setState({ modalOpen: false })
  }

  handleSubmit = (e, { formData }) => {
    e.preventDefault()
    const { id } = this.props
    const { exercise, weight, repetitions } = formData
    this.props.onUpdateClick(id, exercise, weight, repetitions)
    this.handleClose(e)
  }

  render() {
    const { exercise, weight, repetitions } = this.props

    return (
      <Modal
        trigger={<Button onClick={this.handleOpen}>Edit</Button>}
        open={this.state.modalOpen}
      >
        <Modal.Header>Edit Set</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <label>Exercise</label>
              <input
                name="exercise"
                placeholder="Exercise"
                defaultValue={exercise}
                required
              />
            </Form.Field>
            <Form.Field>
              <label>Weight</label>
              <input
                name="weight"
                type="number"
                placeholder='Weight'
                defaultValue={weight}
                min="0"
                required
              />
            </Form.Field>
            <Form.Field>
              <label>Repetitions</label>
              <input
                name="repetitions"
                type="number"
                placeholder='Repetitions'
                defaultValue={repetitions}
                min="1"
                required
              />
            </Form.Field>
            <Button type="submit" floated="right" primary>Update</Button>
            <Button floated="right" onClick={this.handleClose}>Cancel</Button>
            <br />
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

EditModal.propTypes = {
  id: PropTypes.number.isRequired,
  exercise: PropTypes.string.isRequired,
  weight: PropTypes.number.isRequired,
  repetitions: PropTypes.number.isRequired,
  onUpdateClick: PropTypes.func.isRequired
}

export default EditModal