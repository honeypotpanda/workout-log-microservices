import React, { Component } from 'react'
import { Header } from 'semantic-ui-react'

import SetList from '../containers/SetList'
import AddSet from '../containers/AddSet'

const Workout = () => (
  <div>
    <Header size="large">Workout</Header>
    <SetList />
    <AddSet />
  </div>
)

export default Workout