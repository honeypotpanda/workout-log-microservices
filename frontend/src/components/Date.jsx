import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { Menu } from 'semantic-ui-react'

const Date = ({ date, active, onDateClick }) => {
  const [year, month, day] = date.split('-')
  const localDate = `${month}-${day}-${year}`
  return (
    <Menu.Item
      key={date}
      name={localDate}
      active={active}
      onClick={onDateClick}
    />
  )
}

Date.propTypes = {
  date: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onDateClick: PropTypes.func.isRequired
}

export default Date