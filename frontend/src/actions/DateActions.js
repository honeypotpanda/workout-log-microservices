import * as types from '../constants/ActionTypes'

const port = process.env.PORT || 4001

export const addDate = date => ({
  type: types.ADD_DATE,
  date
})

export const selectDate = date => ({
  type: types.SELECT_DATE,
  date
})

export const fetchDatesRequest = () => ({
  type: types.FETCH_DATES_REQUEST
})

export const fetchDatesSuccess = (dates) => ({
  type: types.FETCH_DATES_SUCCESS,
  dates
})

export const fetchDatesFailure = (error) => ({
  type: types.FETCH_DATES_FAILURE,
  error
})

export const fetchDates = () => (dispatch) => {
  dispatch(fetchDatesRequest())
  let statusCode
  return fetch(`http://localhost:${port}/api/workouts/dates`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(response => {
    statusCode = response.status
    return response.json()
  })
  .then(json => {
    if (statusCode >= 200 && statusCode < 300) {
      return dispatch(fetchDatesSuccess(json))
    } else {
      return dispatch(fetchDatesFailure(json))
    }
  })
  .catch(error => {
    return dispatch(fetchDatesFailure(error))
  })
}

export const setDateError = (error) => ({
  type: types.SET_DATE_ERROR,
  error
})

export const resetDateError = () => ({
  type: types.RESET_DATE_ERROR
})

export const setDateErrorTimer = () => ({
  type: types.SET_DATE_ERROR_TIMER
})

export const resetDateErrorTimer = () => ({
  type: types.RESET_DATE_ERROR_TIMER
})