import * as types from '../constants/ActionTypes'

const port = process.env.PORT || 3001

export const fetchAuthRequest = () => ({
  type: types.FETCH_AUTH_REQUEST
})

export const fetchAuthSuccess = () => ({
  type: types.FETCH_AUTH_SUCCESS
})

export const fetchAuthFailure = (error) => ({
  type: types.FETCH_AUTH_FAILURE,
  error
})

export const fetchAuth = (path) => (email, password) => (dispatch) => {
  let statusCode
  if (!email) {
    return Promise.resolve(
      dispatch(fetchAuthFailure({ message: 'Email is required.' }))
    )
  }
  if (!password) {
    return Promise.resolve(
      dispatch(fetchAuthFailure({ message: 'Password is required.' }))
    )
  }
  dispatch(fetchAuthRequest())
  return fetch(`http://localhost:${port}/auth${path}`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: email,
      password: password
    })
  })
  .then(response => {
    statusCode = response.status
    return response.json()
  })
  .then(json => {
    if (statusCode >= 200 && statusCode < 300) {
      localStorage.setItem('token', json.token)
      return dispatch(fetchAuthSuccess(json))
    } else {
      return dispatch(fetchAuthFailure(json))
    }
  })
  .catch(error => {
    return dispatch(fetchAuthFailure(error))
  })
}

export const fetchValidateToken = () => (dispatch) => {
  dispatch(fetchAuthRequest())
  return fetch(`http://localhost:${port}/auth/token`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(response => {
    if (response.status === 200) {
      return dispatch(fetchAuthSuccess())
    } else {
      return response.json()
        .then(json => {
          localStorage.removeItem('token')
          if (json.message === 'UnauthorizedError: jwt malformed') {
            return dispatch(fetchAuthFailure({ message: '' }))
          }
          return dispatch(fetchAuthFailure({ message: 'Invalid token.' }))
        })
        .catch(error => {
          return dispatch(fetchAuthFailure(error))
        })
    }
  })
  .catch(error => {
    return dispatch(fetchAuthFailure(error))
  })
}

export const signOut = () => ({
  type: types.SIGN_OUT
})

export const resetError = () => ({
  type: types.RESET_ERROR
})