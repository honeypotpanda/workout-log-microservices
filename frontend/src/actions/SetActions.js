import * as types from '../constants/ActionTypes'

const port = process.env.PORT || 4001

export const fetchSetsRequest = () => ({
  type: types.FETCH_SETS_REQUEST
})

export const fetchSetsSuccess = (date, sets) => ({
  type: types.FETCH_SETS_SUCCESS,
  date,
  sets
})

export const fetchSetsFailure = (error) => ({
  type: types.FETCH_SETS_FAILURE,
  error
})

export const fetchSets = (date) => (dispatch) => {
  let statusCode
  dispatch(fetchSetsRequest())
  return fetch(`http://localhost:${port}/api/workouts/${date}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(response => {
    statusCode = response.status
    return response.json()
  })
  .then(json => {
    if (statusCode >= 200 && statusCode < 300) {
      return dispatch(fetchSetsSuccess(date, json))
    } else {
      return dispatch(fetchSetsFailure(json))
    }
  })
  .catch(error => {
    return dispatch(fetchSetsFailure(json))
  })
}

export const postSetRequest = () => ({
  type: types.POST_SET_REQUEST
})

export const postSetSuccess = (date, set) => ({
  type: types.POST_SET_SUCCESS,
  date,
  set
})

export const postSetFailure = (error) => ({
  type: types.POST_SET_FAILURE,
  error
})

export const postSet = (date) => (exercise, weight, repetitions) => (dispatch) => {
  let statusCode
  dispatch(postSetRequest())
  return fetch(`http://localhost:${port}/api/workouts`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      date: date,
      exercise: exercise,
      weight: weight,
      repetitions: repetitions
    })
  })
  .then(response => {
    statusCode = response.status
    return response.json()
  })
  .then(json => {
    if (statusCode >= 200 && statusCode < 300) {
      return dispatch(postSetSuccess(date, json))
    } else {
      return dispatch(postSetFailure(json))
    }
  })
  .catch(error => {
    return dispatch(postSetFailure(error))
  })
}

export const deleteSetRequest = () => ({
  type: types.DELETE_SET_REQUEST
})

export const deleteSetSuccess = (date, index) => ({
  type: types.DELETE_SET_SUCCESS,
  date,
  index
})

export const deleteSetFailure = (error) => ({
  type: types.DELETE_SET_FAILURE,
  error
})

export const deleteSet = (date, setId, index) => (dispatch) => {
  let statusCode
  dispatch(deleteSetRequest())
  return fetch(`http://localhost:${port}/api/workouts/${setId}`, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(response => {
    statusCode = response.status
    return response.json()
  })
  .then(json => {
    if (statusCode >= 200 && statusCode < 300) {
      return dispatch(deleteSetSuccess(date, index))
    } else {
      return dispatch(deleteSetFailure(json))
    }
  })
  .catch(error => {
    return dispatch(deleteSetFailure(error))
  })
}

export const putSetRequest = () => ({
  type: types.PUT_SET_REQUEST
})

export const putSetSuccess = (index, exercise, weight, repetitions, date) => ({
  type: types.PUT_SET_SUCCESS,
  index,
  exercise,
  weight,
  repetitions,
  date
})

export const putSetFailure = (error) => ({
  type: types.PUT_SET_FAILURE,
  error
})

export const putSet = (id, index, exercise, weight, repetitions, date) => (dispatch) => {
  let statusCode
  dispatch(putSetRequest())
  return fetch(`http://localhost:${port}/api/workouts/${id}`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      exercise: exercise,
      weight: weight,
      repetitions: repetitions,
      date: date
    })
  })
  .then(response => {
    statusCode = response.status
    return response.json()
  })
  .then(json => {
    if (statusCode >= 200 && statusCode < 300) {
      return dispatch(putSetSuccess(index, exercise, parseInt(weight), parseInt(repetitions), date))
    } else {
      return dispatch(putSetFailure(json))
    }
  })
  .catch(error => {
    return dispatch(putSetFailure(error))
  })
}

export const resetSetsChanged = () => ({
  type: types.RESET_SETS_CHANGED
})