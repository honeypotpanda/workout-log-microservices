require('isomorphic-fetch')
require('semantic-ui-react')
import { AppContainer } from 'react-hot-loader'
import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import App from './containers/App'
import reducer from './reducers'

const loggerMiddleware = createLogger()
let store

if (process.env.NODE_ENV === 'production') {
  store = createStore(
    reducer,
    applyMiddleware(
      thunkMiddleware
    )
  )
} else {
  store = createStore(
    reducer,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
}

render(
  <AppContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </AppContainer>,
  document.getElementById('app')
)

if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const NextApp = require('./containers/App').default
    render(
      <AppContainer>
        <Provider store={store}>
          <App />
        </Provider>
      </AppContainer>,
      document.getElementById('app')
    )
  })
}