import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Menu } from 'semantic-ui-react'

class NavBar extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { isLoggedIn } = this.props
    if (isLoggedIn) {
      return (
        <Menu pointing secondary>
          <Menu.Menu position="right">
            <Menu.Item>
              <Link to="/signout">Sign Out</Link>
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      )
    }
    return (
      <Menu pointing secondary>
        <Menu.Menu position="right">
          <Menu.Item>
            <Link to="/signup">Sign Up</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/signin">Sign In</Link>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}

NavBar.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
  isLoggedIn: state.getIn(['auth', 'isLoggedIn'])
})

export default connect(mapStateToProps)(NavBar)