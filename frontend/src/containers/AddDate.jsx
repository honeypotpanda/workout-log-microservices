import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Menu, Form, Button } from 'semantic-ui-react'

import { addDate, setDateError, resetDateError,
  setDateErrorTimer } from '../actions'

export class AddDate extends Component {
  state = { formData: {} }

  convertToLocalDate = date => {
    const [year, month, day] = date.split('-')
    return `${month}-${day}-${year}`
  }

  handleSubmit = (e, { formData }) => {
    e.preventDefault()
    const { dates, addDate, setDateError, setDateErrorTimer,
      resetDateError } = this.props
    const { date } = formData
    if (dates.includes(date)) {
      setDateErrorTimer()
      setDateError(`${this.convertToLocalDate(date)} already exists`)
    } else {
      addDate(date)
      resetDateError()
    }
  }

  render() {
    return (
      <Menu text vertical>
        <Menu.Item header>Add Date</Menu.Item>
        <Menu.Item>
          <Form onSubmit={this.handleSubmit} size="small">
            <Form.Group inline>
              <Form.Field>
                <input name="date" type="date" min="2016-01-01" max="2017-12-31"
                       onChange={this.handleDateChange} required />
              </Form.Field>
              <Form.Field>
                <Button type="submit">Submit</Button>
              </Form.Field>
            </Form.Group>
          </Form>
        </Menu.Item>
      </Menu>
    )
  }
}

AddDate.propTypes = {
  addDate: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  dates: state.getIn(['dates', 'list'])
})

const mapDispatchToProps = dispatch => ({
  addDate: date => dispatch(addDate(date)),
  setDateError: error => dispatch(setDateError(error)),
  setDateErrorTimer: () => dispatch(setDateErrorTimer()),
  resetDateError: () => dispatch(resetDateError())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddDate)