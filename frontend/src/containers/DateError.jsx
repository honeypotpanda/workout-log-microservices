import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Message } from 'semantic-ui-react'

import { resetDateError, resetDateErrorTimer } from '../actions'

export class DateError extends Component {
  state = { timeoutId: null }

  componentDidUpdate() {
    const { error, resetDateError, shouldResetErrorTimer,
      resetDateErrorTimer } = this.props
    let { timeoutId } = this.state
    if (error && shouldResetErrorTimer) {
      resetDateErrorTimer()
      clearTimeout(timeoutId)
      timeoutId = setTimeout(() => {
        resetDateError()
      }, 5000)
      this.setState({ timeoutId: timeoutId })
    }
  }

  render() {
    const { error } = this.props
    return (
      <Message negative hidden={error === ''}>
        <Message.Header>Unable to add date</Message.Header>
        <p>{error}</p>
      </Message>
    )
  }
}

const mapStateToProps = state => ({
  error: state.getIn(['dates', 'error']),
  shouldResetErrorTimer: state.getIn(['dates', 'shouldResetErrorTimer'])
})

const mapDispatchToProps = dispatch => ({
  resetDateError: () => dispatch(resetDateError()),
  resetDateErrorTimer: () => dispatch(resetDateErrorTimer())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateError)