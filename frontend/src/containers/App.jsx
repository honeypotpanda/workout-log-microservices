import React, { Component, PropTypes } from 'react'
import { Router, Route, browserHistory } from 'react-router'
import { connect } from 'react-redux'

import Home from '../components/Home'
import Signout from '../components/Signout'
import AuthForm from './AuthForm'
import WorkoutLog from './WorkoutLog'

import { fetchValidateToken, signOut, resetError } from '../actions'
import { FETCH_AUTH_FAILURE } from '../constants/ActionTypes'

export class App extends Component {
  constructor(props) {
    super(props)
    this.requireNoAuth = this.requireNoAuth.bind(this)
    this.requireAuth = this.requireAuth.bind(this)
    this.signOut = this.signOut.bind(this)
  }

  requireNoAuth(nextState, replace, next) {
    const { hasFetched, isLoggedIn, fetchValidateToken, resetError } = this.props
    const path = nextState.location.pathname
    if (path === '/signup' || path === '/signin') {
      resetError()
    }
    if (!localStorage.token) {
      return next()
    }
    if (hasFetched && isLoggedIn) {
      replace('/workouts')
      next()
    } else {
      fetchValidateToken()
        .then(action => {
          if (action.type === FETCH_AUTH_FAILURE) {
            next()
          } else {
            replace('/workouts')
            next()
          }
        })
        .catch(error => {
          next()
        })
    }
  }

  requireAuth(nextState, replace, next) {
    if (!localStorage.token) {
      replace('/signin')
      return next()
    }
    const { hasFetched, isLoggedIn, fetchValidateToken } = this.props
    if (hasFetched && isLoggedIn) {
      next()
    } else {
      fetchValidateToken()
        .then(action => {
          if (action.type === FETCH_AUTH_FAILURE) {
            replace('/signin')
            next()
          } else {
            next()
          }
        })
        .catch(error => {
          next()
        })
    }
  }

  signOut(nextState, replace) {
    if (localStorage.token) {
      localStorage.removeItem('token')
    }
    this.props.signOut()
    replace('/')
  }

  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={Home} onEnter={this.requireNoAuth} />
        <Route path="/signin" component={AuthForm} onEnter={this.requireNoAuth} />
        <Route path="/signup" component={AuthForm} onEnter={this.requireNoAuth} />
        <Route path="/signout" component={Signout} onEnter={this.signOut} />
        <Route path="/workouts" component={WorkoutLog} onEnter={this.requireAuth} />
      </Router>
    )
  }
}

App.propTypes = {
  hasFetched: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  fetchValidateToken: PropTypes.func.isRequired,
  resetError: PropTypes.func.isRequired,
  signOut: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  hasFetched: state.getIn(['auth', 'hasFetched']),
  isLoggedIn: state.getIn(['auth', 'isLoggedIn'])
})

const mapDispatchToProps = (dispatch) => ({
  fetchValidateToken: () => dispatch(fetchValidateToken()),
  resetError: () => dispatch(resetError()),
  signOut: () => dispatch(signOut())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)