import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Grid, Container, Header, Segment, Button, Form, Message } from 'semantic-ui-react'
import { Link } from 'react-router'

import { fetchAuth } from '../actions'

class AuthForm extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  state = { formData: {} }

  handleSubmit = (e, { formData }) => {
    e.preventDefault()
    const { onSubmit, location } = this.props
    onSubmit(location.pathname, formData.email, formData.password)
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.isLoggedIn) {
      this.context.router.push('/workouts')
    }
  }

  render() {
    const { isFetching, error, location } = this.props
    const { formData } = this.state
    const path = location.pathname
    return (
      <Container text>
        <br></br>
        <Form onSubmit={this.handleSubmit} loading={isFetching} error={error ? true : false}>
          <Header as="h2" textAlign="center" attached='top'>
            {path === '/signup' ? 'Sign Up' : 'Sign In'}
          </Header>
          <Segment attached>
            <Message
              error
              header='Error'
              content={error}
            />
            <Form.Field>
              <label>Email</label>
              <input type="email" name="email" required />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <input type="password" name="password" required />
            </Form.Field>
            <Button type="submit">Submit</Button>
          </Segment>
        </Form>
        <Header sub textAlign="center">
          <Link to={path === '/signup' ? '/signin' : '/signup'}>
            {path === '/signup' ? 'Sign In' : 'Sign Up'}
          </Link>
        </Header>
      </Container>
    )
  }
}

AuthForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({
  isFetching: state.getIn(['auth', 'isFetching']),
  isLoggedIn: state.getIn(['auth', 'isLoggedIn']),
  error: state.getIn(['auth', 'error'])
})

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (path, email, password) => {
    dispatch(fetchAuth(path)(email, password))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthForm)