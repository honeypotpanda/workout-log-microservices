import { connect } from 'react-redux'
import React, { Component, PropTypes } from 'react'
import { Container, Grid } from 'semantic-ui-react'

import DateList from './DateList'
import AddDate from './AddDate'
import Workout from '../components/Workout'
import NavBar from './NavBar'
import DateError from './DateError'

export class WorkoutLog extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return this.props.isLoggedIn !== nextProps.isLoggedIn ||
           this.props.activeDate !== nextProps.activeDate
  }

  render() {
    if (!this.props.isLoggedIn) {
      return <div>Loading...</div>
    } else {
      return (
        <Container>
          <NavBar />
          <Grid>
            <Grid.Column width={4}>
              <DateError />
              <DateList />
              <AddDate />
            </Grid.Column>
            <Grid.Column stretched width={12}>
              {this.props.activeDate ? <Workout /> : <div></div>}
            </Grid.Column>
          </Grid>
        </Container>
      )
    }
  }
}

WorkoutLog.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  activeDate: PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({
  isLoggedIn: state.getIn(['auth', 'isLoggedIn']),
  activeDate: state.get('activeDate')
})

export default connect(
  mapStateToProps
)(WorkoutLog)