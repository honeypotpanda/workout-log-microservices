import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Form, Header, Button } from 'semantic-ui-react'

import { postSet } from '../actions'

export class AddSet extends Component {
  state = { formData: {} }

  handleSubmit = (e, { formData }) => {
    e.preventDefault()
    const { exercise, weight, repetitions } = formData
    const { activeDate, postSet } = this.props
    postSet(activeDate, exercise, weight, repetitions)
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Header size="large">Add Set</Header>
        <Form.Group inline>
          <Form.Field>
            <input name="exercise" type="text" placeholder="Exercise" required />
          </Form.Field>
          <Form.Field>
            <input name="weight" type="number" placeholder="Weight" min="0" required />
          </Form.Field>
          <Form.Field>
            <input name="repetitions" type="number" placeholder="Repetitions" min="1" required />
          </Form.Field>
          <Form.Field>
            <Button>Submit</Button>
          </Form.Field>
        </Form.Group>
      </Form>
    )
  }
}

AddSet.propTypes = {
  postSet: PropTypes.func.isRequired,
  activeDate: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
  activeDate: state.get('activeDate')
})

const mapDispatchToProps = dispatch => ({
  postSet: (activeDate, exercise, weight, repetitions) => {
    dispatch(postSet(activeDate)(exercise, weight, repetitions))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddSet)