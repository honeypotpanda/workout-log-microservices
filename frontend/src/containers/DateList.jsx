import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { List } from 'immutable'
import { Menu } from 'semantic-ui-react'

import Date from '../components/Date'

import { fetchDates, selectDate } from '../actions'

export class DateList extends Component {
  componentDidMount() {
    this.props.fetchDates()
  }

  render() {
    const { activeDate, dates, onDateClick } = this.props

    if (dates.size === 0) {
      return <div></div>
    }
    return (
      <Menu text vertical>
        <Menu.Item header>Dates</Menu.Item>
        {dates.map(date => (
          <Date
            key={date}
            date={date}
            active={activeDate === date}
            onDateClick={() => onDateClick(date)}
          />
        ))}
      </Menu>
    )
  }
}

DateList.propTypes = {
  fetchDates: PropTypes.func.isRequired,
  onDateClick: PropTypes.func.isRequired,
  activeDate: PropTypes.string.isRequired,
  dates: PropTypes.instanceOf(List)
}

const mapStateToProps = state => ({
  activeDate: state.get('activeDate'),
  dates: state.getIn(['dates', 'list'])
})

const mapDispatchToProps = dispatch => ({
  fetchDates: () => dispatch(fetchDates()),
  onDateClick: date => dispatch(selectDate(date))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateList)