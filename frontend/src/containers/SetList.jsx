import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Map } from 'immutable'
import { Table, Button, Input } from 'semantic-ui-react'

import EditModal from '../components/EditModal'
import DeleteModal from '../components/DeleteModal'
import { fetchSets, editSet, putSet, deleteSet,
  resetSetsChanged } from '../actions'

export class SetList extends Component {
  componentDidMount() {
    this.props.fetchSets(this.props.activeDate)
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { activeDate, setsByDate } = this.props
    return (
      setsByDate.size !== nextProps.setsByDate.size ||
      activeDate !== nextProps.activeDate ||
      nextProps.setsChanged
    )
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.setsChanged) {
      this.props.resetSetsChanged()
    }
    if (!this.props.setsByDate.get(this.props.activeDate)) {
      this.props.fetchSets(this.props.activeDate)
    }
  }

  render() {
    const { setsByDate, activeDate, onUpdateClick, onEditClick,
      onDeleteClick } = this.props
    const sets = setsByDate.get(activeDate)

    if (!sets) {
      return <div></div>
    }

    return (
      <Table basic="very" compact>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Exercise</Table.HeaderCell>
            <Table.HeaderCell>Weight</Table.HeaderCell>
            <Table.HeaderCell>Repetitions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {sets.map((set, index) => {
            const { id, exercise, weight, repetitions } = set.toObject()
            return (
              <Table.Row key={id}>
                <Table.Cell>{exercise}</Table.Cell>
                <Table.Cell>{weight}</Table.Cell>
                <Table.Cell>{repetitions}</Table.Cell>
                <EditModal
                  id={id}
                  exercise={exercise}
                  weight={weight}
                  repetitions={repetitions}
                  onUpdateClick={onUpdateClick(activeDate, index)}
                />
                <DeleteModal
                  onDeleteClick={() => onDeleteClick(activeDate, id, index)}
                />
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
    )
  }
}

SetList.propTypes = {
  fetchSets: PropTypes.func.isRequired,
  activeDate: PropTypes.string.isRequired,
  setsByDate: PropTypes.instanceOf(Map),
  setsChanged: PropTypes.bool.isRequired,
  onUpdateClick: PropTypes.func.isRequired,
  onEditClick: PropTypes.func.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
  resetSetsChanged: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  activeDate: state.get('activeDate'),
  setsByDate: state.getIn(['sets', 'listByDates']),
  setsChanged: state.getIn(['sets', 'setsChanged'])
})

const mapDispatchToProps = dispatch => ({
  fetchSets: date => dispatch(fetchSets(date)),
  onEditClick: index => dispatch(editSet(index)),
  onUpdateClick: (date, index) => (id, exercise, weight, repetitions) =>
    dispatch(putSet(id, index, exercise, weight, repetitions, date)),
  onDeleteClick: (date, setId, index) => dispatch(deleteSet(date, setId, index)),
  resetSetsChanged: () => dispatch(resetSetsChanged())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SetList)