import { Map, List } from 'immutable'
import chai, { expect } from 'chai'
import chaiImmutable from 'chai-immutable'

import reducer from '../../src/reducers/auth'
import * as types from '../../src/constants/ActionTypes'

chai.use(chaiImmutable)

describe('auth reducer', () => {

  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).to.equal(
      Map({
        hasFetched: false,
        isFetching: false,
        isLoggedIn: false,
        error: ''
      })
    )
  })

  it('should handle FETCH_AUTH_REQUEST', () => {
    const stateBefore = Map({
      hasFetched: false,
      isFetching: false,
      isLoggedIn: false,
      error: 'Something went wrong...'
    })
    const action = {
      type: types.FETCH_AUTH_REQUEST
    }
    const stateAfter = Map({
      hasFetched: false,
      isFetching: true,
      isLoggedIn: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_AUTH_SUCCESS', () => {
    const stateBefore = Map({
      hasFetched: false,
      isFetching: true,
      isLoggedIn: false,
      error: ''
    })
    const action = {
      type: types.FETCH_AUTH_SUCCESS
    }
    const stateAfter = Map({
      hasFetched: true,
      isFetching: false,
      isLoggedIn: true,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_AUTH_FAILURE', () => {
    const stateBefore = Map({
      hasFetched: false,
      isFetching: true,
      isLoggedIn: false,
      error: ''
    })
    const action = {
      type: types.FETCH_AUTH_FAILURE,
      error: { message: 'Something went wrong...' }
    }
    const stateAfter = Map({
      hasFetched: true,
      isFetching: false,
      isLoggedIn: false,
      error: 'Something went wrong...'
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle SIGN_OUT', () => {
    const stateBefore = Map({
      hasFetched: true,
      isFetching: true,
      isLoggedIn: true,
      error: 'Something went wrong...'
    })
    const action = {
      type: types.SIGN_OUT
    }
    const stateAfter = Map({
      hasFetched: false,
      isFetching: false,
      isLoggedIn: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle RESET_ERROR', () => {
    const stateBefore = Map({
      hasFetched: false,
      isFetching: false,
      isLoggedIn: false,
      error: 'Something went wrong...'
    })
    const action = {
      type: types.RESET_ERROR
    }
    const stateAfter = Map({
      hasFetched: false,
      isFetching: false,
      isLoggedIn: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

})