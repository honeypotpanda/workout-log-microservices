import { Map, List } from 'immutable'
import chai, { expect } from 'chai'
import chaiImmutable from 'chai-immutable'

import reducer from '../../src/reducers/dates'
import * as types from '../../src/constants/ActionTypes'

chai.use(chaiImmutable)

describe('dates reducer', () => {

  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).to.equal(
      Map({
        isFetching: false,
        list: List(),
        shouldResetErrorTimer: false,
        error: ''
      })
    )
  })

  it('should handle ADD_DATE', () => {
    const stateBefore = Map({
      isFetching: false,
      list: List.of('2016-01-01'),
      shouldResetErrorTimer: false,
      error: ''
    })
    const action = {
      type: types.ADD_DATE,
      date: '2016-01-02'
    }
    const stateAfter = Map({
      isFetching: false,
      list: List.of('2016-01-01', '2016-01-02'),
      shouldResetErrorTimer: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_DATES_REQUEST', () => {
    const stateBefore = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: 'An error occurred...'
    })
    const action = {
      type: types.FETCH_DATES_REQUEST
    }
    const stateAfter = Map({
      isFetching: true,
      list: List(),
      shouldResetErrorTimer: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_DATES_SUCCESS', () => {
    const stateBefore = Map({
      isFetching: true,
      list: List(),
      shouldResetErrorTimer: false,
      error: ''
    })
    const action = {
      type: types.FETCH_DATES_SUCCESS,
      dates: [
        { date: '2016-01-01' },
        { date: '2016-01-02' }
      ]
    }
    const stateAfter = Map({
      isFetching: false,
      list: List.of('2016-01-01', '2016-01-02'),
      shouldResetErrorTimer: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_DATES_FAILURE', () => {
    const stateBefore = Map({
      isFetching: true,
      list: List(),
      shouldResetErrorTimer: false,
      error: ''
    })
    const action = {
      type: types.FETCH_DATES_FAILURE,
      error: { message: 'Something went wrong...' }
    }
    const stateAfter = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: 'Something went wrong...'
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle SIGN_OUT', () => {
    const stateBefore = Map({
      isFetching: true,
      list: List.of('2016-01-01', '2016-01-02'),
      shouldResetErrorTimer: false,
      error: 'Something went wrong...'
    })
    const action = {
      type: types.SIGN_OUT
    }
    const stateAfter = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle SET_DATE_ERROR', () => {
    const stateBefore = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: ''
    })
    const action = {
      type: types.SET_DATE_ERROR,
      error: 'Something went wrong...'
    }
    const stateAfter = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: 'Something went wrong...'
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle RESET_DATE_ERROR', () => {
    const stateBefore = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: 'Something went wrong...'
    })
    const action = {
      type: types.RESET_DATE_ERROR
    }
    const stateAfter = Map({
      isFetching: false,
      list: List(),
      shouldResetErrorTimer: false,
      error: ''
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

})