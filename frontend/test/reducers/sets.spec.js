import { Map, List, fromJS } from 'immutable'
import chai, { expect } from 'chai'
import chaiImmutable from 'chai-immutable'

import reducer from '../../src/reducers/sets'
import * as types from '../../src/constants/ActionTypes'

chai.use(chaiImmutable)

describe('sets reducer', () => {

  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).to.equal(
      Map({
        isFetching: false,
        listByDates: Map(),
        error: '',
        setsChanged: false
      })
    )
  })

  it('should handle FETCH_SETS_REQUEST', () => {
    const stateBefore = Map({
      isFetching: false,
      listByDates: Map(),
      error: 'Something went wrong...',
      setsChanged: false
    })
    const action = {
      type: types.FETCH_SETS_REQUEST
    }
    const stateAfter = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_SETS_SUCCESS', () => {
    const stateBefore = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.FETCH_SETS_SUCCESS,
      date: '2016-01-01',
      sets: [
        { id: 1, exercise: 'Squat', weight: 135, repetitions: 5 },
        { id: 2, exercise: 'Deadlift', weight: 135, repetitions: 5 }
      ]
    }
    const stateAfter = fromJS({
      isFetching: false,
      listByDates: {
        "2016-01-01": [
          { id: 1, exercise: 'Squat', weight: 135, repetitions: 5 },
          { id: 2, exercise: 'Deadlift', weight: 135, repetitions: 5 }
        ]
      },
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle FETCH_SETS_FAILURE', () => {
    const stateBefore = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.FETCH_SETS_FAILURE,
      error: { message: 'Something went wrong...' }
    }
    const stateAfter = fromJS({
      isFetching: false,
      listByDates: {},
      error: 'Something went wrong...',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle POST_SET_REQUEST', () => {
    const stateBefore = Map({
      isFetching: false,
      listByDates: Map(),
      error: 'Something went wrong...',
      setsChanged: false
    })
    const action = {
      type: types.POST_SET_REQUEST
    }
    const stateAfter = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle POST_SET_SUCCESS', () => {
    const stateBefore = fromJS({
      isFetching: true,
      listByDates: {
        "2016-01-01": [
          { id: 1, exercise: 'Squat', weight: 135, repetitions: 5 },
        ]
      },
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.POST_SET_SUCCESS,
      date: '2016-01-01',
      set: { id: 2, exercise: 'Deadlift', weight: 135, repetitions: 5 }
    }
    const stateAfter = fromJS({
      isFetching: false,
      listByDates: {
        "2016-01-01": [
          { id: 1, exercise: 'Squat', weight: 135, repetitions: 5 },
          { id: 2, exercise: 'Deadlift', weight: 135, repetitions: 5 }
        ]
      },
      error: '',
      setsChanged: true
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle POST_SET_FAILURE', () => {
    const stateBefore = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.POST_SET_FAILURE,
      error: { message: 'Something went wrong...' }
    }
    const stateAfter = Map({
      isFetching: false,
      listByDates: Map(),
      error: 'Something went wrong...',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle DELETE_SET_REQUEST', () => {
    const stateBefore = fromJS({
      isFetching: false,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: 'Something went wrong...',
      setsChanged: false
    })
    const action = {
      type: types.DELETE_SET_REQUEST
    }
    const stateAfter = fromJS({
      isFetching: true,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle DELETE_SET_SUCCESS', () => {
    const stateBefore = fromJS({
      isFetching: true,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.DELETE_SET_SUCCESS,
      date: '2016-01-01',
      index: 0
    }
    const stateAfter = fromJS({
      isFetching: false,
      listByDates: {
        '2016-01-01': []
      },
      error: '',
      setsChanged: true
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle DELETE_SET_FAILURE', () => {
    const stateBefore = fromJS({
      isFetching: true,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.DELETE_SET_FAILURE,
      error: { message: 'Something went wrong...' }
    }
    const stateAfter = fromJS({
      isFetching: false,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: 'Something went wrong...',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle PUT_SET_REQUEST', () => {
    const stateBefore = Map({
      isFetching: false,
      listByDates: Map(),
      error: 'Something went wrong...',
      setsChanged: false
    })
    const action = {
      type: types.PUT_SET_REQUEST
    }
    const stateAfter = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle PUT_SET_SUCCESS', () => {
    const stateBefore = fromJS({
      isFetching: true,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.PUT_SET_SUCCESS,
      index: 0,
      exercise: 'Squat',
      weight: 225,
      repetitions: 10,
      date: '2016-01-01'
    }
    const stateAfter = fromJS({
      isFetching: false,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 225,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: '',
      setsChanged: true
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle PUT_SET_FAILURE', () => {
    const stateBefore = Map({
      isFetching: true,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.PUT_SET_FAILURE,
      error: { message: 'Something went wrong...' }
    }
    const stateAfter = Map({
      isFetching: false,
      listByDates: Map(),
      error: 'Something went wrong...',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle RESET_SET_UPDATED', () => {
    const stateBefore = Map({
      isFetching: false,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })
    const action = {
      type: types.RESET_SET_UPDATED
    }
    const stateAfter = Map({
      isFetching: false,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle RESET_SETS_CHANGED', () => {
    const stateBefore = Map({
      isFetching: false,
      listByDates: Map(),
      error: '',
      setsChanged: true
    })
    const action = {
      type: types.RESET_SETS_CHANGED
    }
    const stateAfter = Map({
      isFetching: false,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle SIGN_OUT', () => {
    const stateBefore = fromJS({
      isFetching: true,
      listByDates: {
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 135,
            repetitions: 5
          }
        ]
      },
      error: 'Something went wrong...',
      setsChanged: true
    })
    const action = {
      type: types.SIGN_OUT
    }
    const stateAfter = Map({
      isFetching: false,
      listByDates: Map(),
      error: '',
      setsChanged: false
    })

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

})