import { expect } from 'chai'

import reducer from '../../src/reducers/activeDate'
import * as types from '../../src/constants/ActionTypes'

describe('activeDate reducer', () => {

  it('should return the inital state', () => {
    expect(
      reducer(undefined, {})
    ).to.equal('')
  })

  it('should handle SELECT_DATE', () => {
    const stateBefore = ''
    const action = {
      type: types.SELECT_DATE,
      date: '2016-01-01'
    }
    const stateAfter = '2016-01-01'

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

  it('should handle SIGN_OUT', () => {
    const stateBefore = '2016-01-01'
    const action = {
      type: types.SIGN_OUT
    }
    const stateAfter = ''

    expect(
      reducer(stateBefore, action)
    ).to.equal(stateAfter)
  })

})