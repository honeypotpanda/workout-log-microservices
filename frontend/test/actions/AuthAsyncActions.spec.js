import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../src/actions/AuthActions'
import * as types from '../../src/constants/ActionTypes'
import nock from 'nock'
import expect from 'expect'
import { Map } from 'immutable'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const initialState = {
  auth: Map({
    isFetching: false,
    isLoggedIn: false,
    error: ''
  })
}
const postSignup = actions.fetchAuth('/signup')
const email = 'test@test.com'
const password = 'test'

describe('auth async actions', () => {

  afterEach(() => {
    nock.cleanAll()
  })

  it('creates FETCH_AUTH_SUCCESS when signup is completed', () => {
    nock('http://localhost:3001')
      .post('/auth/signup')
      .reply(200, { token: 'token' })

    const expectedActions = [
      { type: types.FETCH_AUTH_REQUEST },
      { type: types.FETCH_AUTH_SUCCESS }
    ]
    const store = mockStore(initialState)

    return store.dispatch(postSignup(email, password))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_AUTH_FAILURE when email is missing', () => {
    const expectedActions = [
      { type: types.FETCH_AUTH_FAILURE, error: { message: 'Email is required.' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(postSignup('', password))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_AUTH_FAILURE when password is missing', () => {
    const expectedActions = [
      { type: types.FETCH_AUTH_FAILURE, error: { message: 'Password is required.' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(postSignup(email, ''))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_AUTH_FAILURE when there is an error signing up', () => {
    nock('http://localhost:3001')
      .post('/auth/signup')
      .reply(400, { message: 'Error...' })

    const expectedActions = [
      { type: types.FETCH_AUTH_REQUEST },
      { type: types.FETCH_AUTH_FAILURE, error: { message: 'Error...' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(postSignup(email, password))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_AUTH_SUCCESS for a valid token', () => {
    nock('http://localhost:3001')
      .get('/auth/token')
      .reply(200)

    const expectedActions = [
      { type: types.FETCH_AUTH_REQUEST },
      { type: types.FETCH_AUTH_SUCCESS }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchValidateToken())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_AUTH_FAILURE for an invalid token', () => {
    nock('http://localhost:3001')
      .get('/auth/token')
      .reply(401, { message: 'UnauthorizedError: invalid token' })

    const expectedActions = [
      { type: types.FETCH_AUTH_REQUEST },
      { type: types.FETCH_AUTH_FAILURE, error: { message: 'Invalid token.' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchValidateToken())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_AUTH_FAILURE for no token', () => {
    nock('http://localhost:3001')
      .get('/auth/token')
      .reply(401, { message: 'UnauthorizedError: jwt malformed' })

    const expectedActions = [
      { type: types.FETCH_AUTH_REQUEST },
      { type: types.FETCH_AUTH_FAILURE, error: { message: '' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchValidateToken())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

})