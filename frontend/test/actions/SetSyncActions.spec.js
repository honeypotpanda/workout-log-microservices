import * as actions from '../../src/actions/SetActions'
import * as types from '../../src/constants/ActionTypes'
import expect from 'expect'

describe('set sync actions', () => {

  it('fetchSetsRequest should create FETCH_SETS_REQUEST action', () => {
    expect(actions.fetchSetsRequest()).toEqual({
      type: types.FETCH_SETS_REQUEST
    })
  })

  it('fetchSetsSuccess should create FETCH_SETS_SUCCESS action', () => {
    const sets = [{ id: 1, exercise: 'Squat', weight: 135, repetitions: 5 }]
    expect(actions.fetchSetsSuccess('2016-01-01', sets)).toEqual({
      type: types.FETCH_SETS_SUCCESS,
      date: '2016-01-01',
      sets: [
        {
          id: 1,
          exercise: 'Squat',
          weight: 135,
          repetitions: 5
        }
      ]
    })
  })

  it('fetchSetsFailure should create FETCH_SETS_FAILURE action', () => {
    expect(actions.fetchSetsFailure({ message: 'Something went wrong...' })).toEqual({
      type: types.FETCH_SETS_FAILURE,
      error: {
        message: 'Something went wrong...'
      }
    })
  })

  it('postSetRequest should create POST_SET_REQUEST', () => {
    expect(actions.postSetRequest()).toEqual({
      type: types.POST_SET_REQUEST
    })
  })

  it('postSetSuccess should create POST_SET_SUCCESS', () => {
    const date = '2016-01-01'
    const set = {
      id: 1,
      exercise: 'Squat',
      weight: 135,
      repetitions: 5
    }
    expect(actions.postSetSuccess(date, set)).toEqual({
      type: types.POST_SET_SUCCESS,
      date: '2016-01-01',
      set: {
        id: 1,
        exercise: 'Squat',
        weight: 135,
        repetitions: 5
      }
    })
  })

  it('postSetFailure should create POST_SET_FAILURE', () => {
    expect(actions.postSetFailure({ message: 'Something went wrong...' })).toEqual({
      type: types.POST_SET_FAILURE,
      error: {
        message: 'Something went wrong...'
      }
    })
  })

  it('deleteSetRequest should create DELETE_SET_REQUEST', () => {
    expect(actions.deleteSetRequest()).toEqual({
      type: types.DELETE_SET_REQUEST
    })
  })

  it('deleteSetSuccess should create DELETE_SET_SUCCESS', () => {
    expect(actions.deleteSetSuccess('2016-01-01', 0)).toEqual({
      type: types.DELETE_SET_SUCCESS,
      date: '2016-01-01',
      index: 0
    })
  })

  it('deleteSetFailure should create DELETE_SET_FAILURE', () => {
    expect(actions.deleteSetFailure({ message: 'Something went wrong...' })).toEqual({
      type: types.DELETE_SET_FAILURE,
      error: {
        message: 'Something went wrong...'
      }
    })
  })

  it('putSetRequest should create PUT_SET_REQUEST', () => {
    expect(actions.putSetRequest()).toEqual({
      type: types.PUT_SET_REQUEST
    })
  })

  it('putSetSuccess should create PUT_SET_SUCCESS', () => {
    expect(actions.putSetSuccess(0, 'Squat', 135, 5, '2016-01-01')).toEqual({
      type: types.PUT_SET_SUCCESS,
      index: 0,
      exercise: 'Squat',
      weight: 135,
      repetitions: 5,
      date: '2016-01-01'
    })
  })

  it('putSetFailure should create PUT_SET_FAILURE', () => {
    expect(actions.putSetFailure({ message: 'Something went wrong...' })).toEqual({
      type: types.PUT_SET_FAILURE,
      error: { message: 'Something went wrong...' }
    })
  })

  it('resetSetsChanged should create RESET_SETS_CHANGED', () => {
    expect(actions.resetSetsChanged()).toEqual({
      type: types.RESET_SETS_CHANGED
    })
  })

})