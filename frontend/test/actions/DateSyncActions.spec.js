import * as actions from '../../src/actions/DateActions'
import * as types from '../../src/constants/ActionTypes'
import expect from 'expect'

describe('date actions', () => {

  it('addDate should create ADD_DATE action', () => {
    expect(actions.addDate('2016-01-01')).toEqual({
      type: types.ADD_DATE,
      date: '2016-01-01'
    })
  })

  it('selectDate should create SELECT_DATE action', () => {
    expect(actions.selectDate('2016-01-01')).toEqual({
      type: types.SELECT_DATE,
      date: '2016-01-01'
    })
  })

  it('fetchDatesRequest should create FETCH_DATES_REQUEST action', () => {
    expect(actions.fetchDatesRequest()).toEqual({
      type: types.FETCH_DATES_REQUEST
    })
  })

  it('fetchDatesSuccess should create FETCH_DATES_SUCCESS action', () => {
    expect(actions.fetchDatesSuccess(['2016-01-01'])).toEqual({
      type: types.FETCH_DATES_SUCCESS,
      dates: ['2016-01-01']
    })
  })

  it('fetchDatesFailure should create FETCH_DATES_FAILURE action', () => {
    expect(actions.fetchDatesFailure('error')).toEqual({
      type: types.FETCH_DATES_FAILURE,
      error: 'error'
    })
  })

  it('setDateError should create SET_DATE_ERROR action', () => {
    expect(actions.setDateError('Something went wrong...')).toEqual({
      type: types.SET_DATE_ERROR,
      error: 'Something went wrong...'
    })
  })

  it('resetDateError should create RESET_DATE_ERROR action', () => {
    expect(actions.resetDateError()).toEqual({
      type: types.RESET_DATE_ERROR
    })
  })

  it('setDateErrorTimer should create SET_DATE_ERROR_TIMER', () => {
    expect(actions.setDateErrorTimer()).toEqual({
      type: types.SET_DATE_ERROR_TIMER
    })
  })

  it('resetDateErrorTimer should create RESET_DATE_ERROR_TIMER', () => {
    expect(actions.resetDateErrorTimer()).toEqual({
      type: types.RESET_DATE_ERROR_TIMER
    })
  })

})