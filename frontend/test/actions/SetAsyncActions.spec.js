import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../src/actions/SetActions'
import * as types from '../../src/constants/ActionTypes'
import nock from 'nock'
import expect from 'expect'
import { Map, List } from 'immutable'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const initialState = {
  sets: Map({
    isFetching: false,
    listByDate: List(),
    error: ''
  })
}
const sets = [
  {
    id: 1,
    exercise: 'Squat',
    weight: 135,
    repetitions: 5
  },
  {
    id: 2,
    exercise: 'Deadlift',
    weight: 225,
    repetitions: 5
  }
]

describe('set async actions', () => {

  afterEach(() => {
    nock.cleanAll()
  })

  it('creates FETCH_SETS_SUCCESS when fetching sets has succeeded', () => {
    nock('http://localhost:4001')
      .get('/api/workouts/2016-01-01')
      .reply(200, sets)

    const expectedActions = [
      { type: types.FETCH_SETS_REQUEST },
      { type: types.FETCH_SETS_SUCCESS, date: '2016-01-01', sets: sets }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchSets('2016-01-01'))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_SETS_FAILURE when fetching sets has failed', () => {
    nock('http://localhost:4001')
      .get('/api/workouts/2016-01-01')
      .reply(400, { message: 'Something went wrong...' })

    const expectedActions = [
      { type: types.FETCH_SETS_REQUEST },
      { type: types.FETCH_SETS_FAILURE, error: { message: 'Something went wrong...' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchSets('2016-01-01'))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates POST_SET_SUCCESS when posting set has succeeded', () => {
    const set = {
      id: 1,
      exercise: 'Squat',
      weight: 135,
      repetitions: 5
    }

    nock('http://localhost:4001')
      .post('/api/workouts')
      .reply(200, set)

    const expectedActions = [
      { type: types.POST_SET_REQUEST },
      { type: types.POST_SET_SUCCESS, date: '2016-01-01', set: set }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.postSet('2016-01-01')('Squat', 135, 5))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates POST_SET_FAILURE when posting set has failed', () => {
    nock('http://localhost:4001')
      .post('/api/workouts')
      .reply(400, { message: 'Something went wrong...' })

    const expectedActions = [
      { type: types.POST_SET_REQUEST },
      { type: types.POST_SET_FAILURE, error: { message: 'Something went wrong...' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.postSet('2016-01-01')('Squat', 135, 5))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates DELETE_SET_SUCCESS when deleting set has succeeded', () => {
    const date = '2016-01-01'
    const setId = 1
    const index = 0

    nock('http://localhost:4001')
      .delete(`/api/workouts/${setId}`)
      .reply(200, {})

    const expectedActions = [
      { type: types.DELETE_SET_REQUEST },
      { type: types.DELETE_SET_SUCCESS, date: '2016-01-01', index: 0 }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.deleteSet(date, setId, index))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates DELETE_SET_SUCCESS when deleting set has failed', () => {
    const date = '2016-01-01'
    const setId = 1
    const index = 0

    nock('http://localhost:4001')
      .delete(`/api/workouts/${setId}`)
      .reply(404, { message: 'Unable to delete workout set.' })

    const expectedActions = [
      { type: types.DELETE_SET_REQUEST },
      { type: types.DELETE_SET_FAILURE, error: { message: 'Unable to delete workout set.' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.deleteSet(date, setId, index))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates PUT_SET_SUCCESS after successful put set', () => {
    const setId = 1
    const index = 0
    const exercise = 'Squat'
    const weight = 135
    const repetitions = 5
    const date = '2016-01-01'
    const response = {
      id: setId,
      exercise: exercise,
      weight: weight,
      repetitions: repetitions,
      date: date
    }

    nock('http://localhost:4001')
      .put(`/api/workouts/${setId}`)
      .reply(200, response)

    const expectedActions = [
      { type: types.PUT_SET_REQUEST },
      {
        type: types.PUT_SET_SUCCESS,
        index: index,
        exercise: exercise,
        weight: weight,
        repetitions: repetitions,
        date: date
      }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.putSet(setId, index, exercise, weight, repetitions, date))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates PUT_SET_FAILURE after unsuccessful put set', () => {
    const setId = 1
    const index = 0
    const exercise = 'Squat'
    const weight = 135
    const repetitions = 5
    const date = '2016-01-01'

    nock('http://localhost:4001')
      .put(`/api/workouts/${setId}`)
      .reply(400, { message: 'Something went wrong...' })

    const expectedActions = [
      { type: types.PUT_SET_REQUEST },
      { type: types.PUT_SET_FAILURE, error: { message: 'Something went wrong...' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.putSet(setId, index, exercise, weight, repetitions, date))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

})