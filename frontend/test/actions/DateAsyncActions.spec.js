import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../src/actions/DateActions'
import * as types from '../../src/constants/ActionTypes'
import nock from 'nock'
import expect from 'expect'
import { Map, List } from 'immutable'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const initialState = {
  dates: Map({
    isFetching: false,
    list: List(),
    error: ''
  })
}

describe('date async actions', () => {

  afterEach(() => {
    nock.cleanAll()
  })

  it('creates FETCH_DATES_SUCCESS when fetching dates has been done', () => {
    nock('http://localhost:4001')
      .get('/api/workouts/dates')
      .reply(200, ['2016-01-01'])

    const expectedActions = [
      { type: types.FETCH_DATES_REQUEST },
      { type: types.FETCH_DATES_SUCCESS, dates: ['2016-01-01'] }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchDates())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

  it('creates FETCH_DATES_FAILURE when there is an error fetching dates', () => {
    nock('http://localhost:4001')
      .get('/api/workouts/dates')
      .reply(400, { message: 'Error...' })

    const expectedActions = [
      { type: types.FETCH_DATES_REQUEST },
      { type: types.FETCH_DATES_FAILURE, error: { message: 'Error...' } }
    ]
    const store = mockStore(initialState)

    return store.dispatch(actions.fetchDates())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })

})