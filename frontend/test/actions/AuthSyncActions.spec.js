import * as actions from '../../src/actions/AuthActions'
import * as types from '../../src/constants/ActionTypes'
import expect from 'expect'

describe('auth sync actions', () => {

  it('fetchAuthRequest should create FETCH_AUTH_REQUEST action', () => {
    expect(actions.fetchAuthRequest()).toEqual({
      type: types.FETCH_AUTH_REQUEST
    })
  })

  it('fetchAuthSuccess should create FETCH_AUTH_SUCCESS action', () => {
    expect(actions.fetchAuthSuccess()).toEqual({
      type: types.FETCH_AUTH_SUCCESS
    })
  })

  it('fetchAuthFailure should create FETCH_AUTH_FAILURE action', () => {
    expect(actions.fetchAuthFailure({ message: 'Something went wrong...' })).toEqual({
      type: types.FETCH_AUTH_FAILURE,
      error: {
        message: 'Something went wrong...'
      }
    })
  })

  it('signOut should create SIGN_OUT action', () => {
    expect(actions.signOut()).toEqual({
      type: types.SIGN_OUT
    })
  })

  it('resetError should create RESET_ERROR action', () => {
    expect(actions.resetError()).toEqual({
      type: types.RESET_ERROR
    })
  })

})