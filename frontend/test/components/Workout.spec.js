import React from 'react'
import { shallow, mount } from 'enzyme'
import { expect } from 'chai'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { fromJS } from 'immutable'
import thunk from 'redux-thunk'

import Workout from '../../src/components/Workout'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const initialState = fromJS({
  activeDate: '2016-01-01',
  sets: {
    listByDates: {}
  },
  editingIndex: -1
})
const store = mockStore(initialState)

describe('<Workout />', () => {

  it('renders <h2>', () => {
    const wrapper = shallow(<Workout />)
    const headerProps = wrapper.find('h2').props()
    expect(headerProps.children).to.equal('Workout')
  })

  it('renders <SetList />', () => {
    const wrapper = mount(
      <Provider store={store}>
        <Workout />
      </Provider>
    )
    expect(wrapper.find('SetList')).to.have.length(1)
  })

  it('renders <AddSet />', () => {
    const wrapper = mount(
      <Provider store={store}>
        <Workout />
      </Provider>
    )
    expect(wrapper.find('AddSet')).to.have.length(1)
  })

})