import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'

import EditSet from '../../src/components/EditSet'

function setup() {
  const props = {
    id: 1,
    exercise: 'Squat',
    weight: 135,
    repetitions: 5,
    onUpdateClick: sinon.spy(),
    onCancelClick: sinon.spy()
  }

  const wrapper = shallow(<EditSet {...props} />)

  return {
    props,
    wrapper
  }
}

describe('<EditSet />', () => {

  it('renders three <input> elements', () => {
    const { wrapper } = setup()
    const [input1, input2, input3] = wrapper.find('input')
    const expectedProps1 = {
      placeholder: 'Exercise',
      defaultValue: 'Squat',
      required: true,
      onChange: input1.props.onChange
    }
    const expectedProps2 = {
      placeholder: 'Weight',
      defaultValue: 135,
      required: true,
      min: '0',
      type: 'number',
      onChange: input2.props.onChange
    }
    const expectedProps3 = {
      placeholder: 'Repetitions',
      defaultValue: 5,
      required: true,
      min: '1',
      type: 'number',
      onChange: input3.props.onChange
    }

    expect(wrapper.find('input')).to.have.length(3)
    expect(input1.props).to.deep.equal(expectedProps1)
    expect(input2.props).to.deep.equal(expectedProps2)
    expect(input3.props).to.deep.equal(expectedProps3)
  })

  it('renders two <button> elements', () => {
    const { wrapper } = setup()
    const [updateButton, cancelButton] = wrapper.find('button')
    expect(wrapper.find('button')).to.have.length(2)
    expect(updateButton.props.onClick).to.exist;
    expect(updateButton.props.children).to.equal('Update')
    expect(cancelButton.props.onClick).to.exist;
    expect(cancelButton.props.children).to.equal('Cancel')
  })

  it('simulates click events', () => {
    const { props, wrapper } = setup()
    const {
      id, exercise, weight, repetitions, onUpdateClick, onCancelClick
    } = props

    wrapper.find('button').forEach(node => {
      node.simulate('click', { preventDefault() {} })
    })
    expect(onUpdateClick.calledOnce).to.be.true
    expect(onUpdateClick.calledWithExactly(id, exercise, weight, repetitions)).to.be.true
    expect(onCancelClick.calledOnce).to.be.true
  })

})