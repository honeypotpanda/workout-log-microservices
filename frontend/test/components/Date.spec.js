import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'

import Date from '../../src/components/Date'

function setup() {
  const props = {
    date: '2016-01-01',
    onDateClick: sinon.spy()
  }

  const wrapper = shallow(<Date {...props} />)

  return {
    props,
    wrapper
  }
}

describe('<Date />', () => {

  it('renders one <li> element', () => {
    const { wrapper } = setup()
    expect(wrapper.find('li')).to.have.length(1)
  })

  it('renders one <Link /> component inside <li> element', () => {
    const { wrapper } = setup()
    expect(wrapper.find('Link')).to.have.length(1)
  })

  it('simulates click event', () => {
    const { props, wrapper } = setup()
    const { onDateClick } = props
    wrapper.find('Link').simulate('click')
    expect(onDateClick).to.have.property('callCount', 1)
  })

})