import React from 'react'
import { shallow, mount } from 'enzyme'
import { expect } from 'chai'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { fromJS } from 'immutable'
import thunk from 'redux-thunk'
import sinon from 'sinon'

import { WorkoutLog } from '../../src/containers/WorkoutLog'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const initialState = fromJS({
  auth: {
    isLoggedIn: true
  },
  activeDate: '2016-01-01',
  dates: {
    list: []
  },
  sets: {
    listByDates: {}
  },
  editingIndex: -1
})
const store = mockStore(initialState)

describe('<WorkoutLog />', () => {

  it('renders loading if not logged in', () => {
    const props = {
      isLoggedIn: false,
      activeDate: '2016-01-01'
    }
    const wrapper = shallow(<WorkoutLog {...props} />)
    const loadingDivProps = wrapper.find('div').props()
    expect(loadingDivProps.children).to.equal('Loading...')
  })

  it('renders <h1>', () => {
    const props = {
      isLoggedIn: true,
      activeDate: '2016-01-01'
    }
    const wrapper = shallow(<WorkoutLog {...props} />)
    const headerProps = wrapper.find('h1').props()
    expect(headerProps.children).to.equal('Workout Log')
  })

  it('renders <DateList />', () => {
    const props = {
      isLoggedIn: true,
      activeDate: ''
    }
    const wrapper = mount(
      <Provider store={store}>
        <WorkoutLog {...props} />
      </Provider>
    )
    expect(wrapper.find('DateList')).to.have.length(1)
  })

  it('renders <AddDate />', () => {
    const props = {
      isLoggedIn: true,
      activeDate: ''
    }
    const wrapper = mount(
      <Provider store={store}>
        <WorkoutLog {...props} />
      </Provider>
    )
    expect(wrapper.find('AddDate')).to.have.length(1)
  })

  it('does not render <Workout /> if there is no activeDate', () => {
    const props = {
      isLoggedIn: true,
      activeDate: ''
    }
    const wrapper = mount(
      <Provider store={store}>
        <WorkoutLog {...props} />
      </Provider>
    )
    expect(wrapper.find('Workout')).to.have.length(0)
  })

  it('renders <Workout /> if there is an activeDate', () => {
    const props = {
      isLoggedIn: true,
      activeDate: '2016-01-01'
    }
    const wrapper = mount(
      <Provider store={store}>
        <WorkoutLog {...props} />
      </Provider>
    )
    expect(wrapper.find('Workout')).to.have.length(1)
  })

  // TODO
  it('calls shouldComponentUpdate', () => {

  })

})