import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import ActiveDate from '../../src/components/ActiveDate'

describe('<ActiveDate />', () => {

  it('it renders one <li> element with localDate', () => {
    const wrapper = shallow(<ActiveDate date='2016-01-01' />)
    expect(wrapper.find('li').text()).to.equal('01-01-2016')
  })

})