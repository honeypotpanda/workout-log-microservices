import React from 'react'
import { shallow, mount } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'
import { List, Map, fromJS } from 'immutable'

import { SetList } from '../../src/containers/SetList'

describe('<SetList />', () => {

  it('renders two <Set />', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: -1,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    const wrapper = shallow(<SetList {...props} />)
    expect(wrapper.find('Set').length).to.equal(2)
    const firstSetProps = wrapper.find('Set').at(0).props()
    expect(firstSetProps.exercise).to.equal('Squat')
    expect(firstSetProps.showExercise).to.be.true
    expect(firstSetProps.weight).to.equal(135)
    expect(firstSetProps.repetitions).to.equal(10)
  })

  it('renders one <EditSet /> and one <Set />', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    const wrapper = shallow(<SetList {...props} />)
    expect(wrapper.find('EditSet').length).to.equal(1)
    expect(wrapper.find('Set').length).to.equal(1)
    const editSetProps = wrapper.find('EditSet').props()
    expect(editSetProps.id).to.equal(1)
    expect(editSetProps.exercise).to.equal('Squat')
    expect(editSetProps.weight).to.equal(135)
    expect(editSetProps.repetitions).to.equal(10)
    expect(editSetProps.onUpdateClick).to.exist
    expect(editSetProps.onCancelClick).to.exist
  })

  it('renders <button> for edit', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    const wrapper = shallow(<SetList {...props} />)
    const editButtonProps = wrapper.find('button').at(0).props()
    expect(editButtonProps.onClick).to.exist
    expect(editButtonProps.children).to.equal('Edit')
  })

  it('renders <button> for delete', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    const wrapper = shallow(<SetList {...props} />)
    const deleteButtonProps = wrapper.find('button').at(1).props()
    expect(deleteButtonProps.onClick).to.exist
    expect(deleteButtonProps.children).to.equal('Delete')
  })

  it('simulates click events', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    const wrapper = shallow(<SetList {...props} />)
    wrapper.find('button').forEach(node => {
      node.simulate('click')
    })
    expect(props.onEditClick.calledOnce).to.be.true
    expect(props.onDeleteClick.calledOnce).to.be.true
  })

  it('calls componentDidMount', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    sinon.spy(SetList.prototype, 'componentDidMount')
    const wrapper = mount(<SetList {...props} />)
    expect(SetList.prototype.componentDidMount.calledOnce).to.be.true
    expect(props.fetchSets.calledOnce).to.be.true
    expect(props.fetchSets.calledWithExactly(props.activeDate))
  })

  it('calls shouldComponentUpdate', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ],
        '2016-01-02': []
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    const newSetsByDate = fromJS({
      '2016-01-01': [
        {
          id: 1,
          exercise: 'Squat',
          weight: 135,
          repetitions: 10
        },
        {
          id: 2,
          exercise: 'Deadlift',
          weight: 225,
          repetitions: 5
        }
      ],
      '2016-01-02': [
        {
          id: 3,
          exercise: 'Deadlift',
          weight: 225,
          repetitions: 5
        }
      ]
    })
    sinon.spy(SetList.prototype, 'shouldComponentUpdate')
    const wrapper = mount(<SetList {...props} />)
    wrapper.setProps({ activeDate: '2016-01-02' })
    wrapper.setProps({ editingIndex: -1 })
    wrapper.setProps({ setsByDate: newSetsByDate })
    expect(SetList.prototype.shouldComponentUpdate.calledThrice)
    expect(SetList.prototype.shouldComponentUpdate.returnValues).to.deep.equal([true, true, true])
  })

  it('calls componentDidUpdate', () => {
    const props = {
      fetchSets: sinon.spy(),
      activeDate: '2016-01-01',
      editingIndex: 0,
      setsByDate: fromJS({
        '2016-01-01': [
          {
            id: 1,
            exercise: 'Squat',
            weight: 135,
            repetitions: 10
          },
          {
            id: 2,
            exercise: 'Deadlift',
            weight: 225,
            repetitions: 5
          }
        ]
      }),
      onUpdateClick: sinon.stub().returns(sinon.spy()),
      onEditClick: sinon.spy(),
      onDeleteClick: sinon.spy(),
      onCancelClick: sinon.spy()
    }
    sinon.spy(SetList.prototype, 'componentDidUpdate')
    const wrapper = mount(<SetList {...props} />)
    wrapper.setProps({ activeDate: '2016-01-02' })
    expect(SetList.prototype.componentDidUpdate.calledOnce).to.be.true
    expect(props.fetchSets.calledTwice).to.be.true
  })

})