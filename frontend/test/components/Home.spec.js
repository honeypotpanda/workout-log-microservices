import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'

import Home from '../../src/components/Home'

describe('<Home />', () => {

  it('renders <h1>', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find('h1').text()).to.equal('Welcome!')
  })

  it('renders <div>', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find('div').get(1)).to.exist
  })

  it('renders <Link> for sign up', () => {
    const wrapper = shallow(<Home />)
    const signupLink = wrapper.find('Link').get(0)
    expect(signupLink.props.to).to.equal('/signup')
    expect(signupLink.props.children).to.equal('sign up')
  })

  it('renders <Link> for sign in', () => {
    const wrapper = shallow(<Home />)
    const signinLink = wrapper.find('Link').get(1)
    expect(signinLink.props.to).to.equal('/signin')
    expect(signinLink.props.children).to.equal('sign in')
  })

})