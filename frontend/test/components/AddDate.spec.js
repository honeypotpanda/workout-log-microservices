import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'

import { AddDate } from '../../src/containers/AddDate'

describe('<AddDate />', () => {

  it('renders <form>', () => {
    const props = {
      dispatch: sinon.spy()
    }
    const wrapper = shallow(<AddDate {...props} />)
    const formProps = wrapper.find('form').props()
    expect(formProps.onSubmit).to.exist
  })

  it('renders <input> for date', () => {
    const props = {
      dispatch: sinon.spy()
    }
    const wrapper = shallow(<AddDate {...props} />)
    const dateInputProps = wrapper.find('input').props()
    expect(dateInputProps.type).to.equal('date')
    expect(dateInputProps.min).to.equal('2016-01-01')
    expect(dateInputProps.max).to.equal('2017-12-31')
    expect(dateInputProps.onChange).to.exist
    expect(dateInputProps.required).to.be.true
  })

  it('renders <button>', () => {
    const props = {
      dispatch: sinon.spy()
    }
    const wrapper = shallow(<AddDate {...props} />)
    const buttonProps = wrapper.find('button').props()
    expect(buttonProps.type).to.equal('submit')
    expect(buttonProps.children).to.equal('Add Date')
  })

  it('simulates submit event', () => {
    const props = {
      dispatch: sinon.spy()
    }
    const wrapper = shallow(<AddDate {...props} />)
      .setState({ date: '2016-01-01' })
    const form = wrapper.find('form')
    expect(wrapper.state('date')).to.equal('2016-01-01')
    form.simulate('submit', { preventDefault() {} })
    expect(props.dispatch.calledOnce).to.be.true
    expect(wrapper.state('date')).to.equal(null)
  })

})