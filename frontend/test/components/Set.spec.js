import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import Set from '../../src/components/Set'

describe('<Set />', () => {

  it('renders <h3>', () => {
    const props = {
      showExercise: true,
      exercise: 'Squat',
      weight: 135,
      repetitions: 5,
    }
    let wrapper = shallow(<Set {...props} />)
    expect(wrapper.find('h3').prop('children')).to.equal('Squat')
    props.showExercise = false
    wrapper = shallow(<Set {...props} />)
    expect(wrapper.find('h3').prop('children')).to.equal('')
  })

  it('renders <span> for weight and repetitions', () => {
    const props = {
      showExercise: true,
      exercise: 'Squat',
      weight: 135,
      repetitions: 5,
    }
    let wrapper = shallow(<Set {...props} />)
    expect(wrapper.find('span').at(1).prop('children').join('')).to.equal('135 x 5')
  })

})