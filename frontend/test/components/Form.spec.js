import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'

import Form from '../../src/components/Form'

describe('<Form />', () => {

  it('renders <form>', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    const wrapper = shallow(<Form {...props} />, { context })
    const formProps = wrapper.find('form').props()
    expect(wrapper.find('form')).to.have.length(1)
    expect(formProps.onSubmit).to.be.exist
  })

  it('renders <h2> for path', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    const wrapper = shallow(<Form {...props} />, { context })
    expect(wrapper.find('h2').text()).to.equal('Sign up')
  })

  it('renders <div> for isFetching', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    let wrapper = shallow(<Form {...props} />, { context })
    expect(wrapper.find('#isFetching').text()).to.equal('')
    props.isFetching = true
    wrapper = shallow(<Form {...props} />, { context })
    expect(wrapper.find('#isFetching').text()).to.equal('Loading...')
  })

  it('renders <div> for error', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    let wrapper = shallow(<Form {...props} />, { context })
    expect(wrapper.find('#error').text()).to.equal('')
    props.error = 'Something went wrong...'
    wrapper = shallow(<Form {...props} />, { context })
    expect(wrapper.find('#error').text()).to.equal('Something went wrong...')
  })

  it('renders <input> for email', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    const wrapper = shallow(<Form {...props} />, { context })
    const emailInputProps = wrapper.find('input').get(0).props
    expect(emailInputProps.type).to.equal('email')
    expect(emailInputProps.name).to.equal('email')
    expect(emailInputProps.onChange).to.exist
    expect(emailInputProps.required).to.be.true
  })

  it('renders <input> for password', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    const wrapper = shallow(<Form {...props} />, { context })
    const passwordInputProps = wrapper.find('input').get(1).props
    expect(passwordInputProps.type).to.equal('password')
    expect(passwordInputProps.name).to.equal('password')
    expect(passwordInputProps.onChange).to.exist
    expect(passwordInputProps.required).to.be.true
  })

  it('renders <button> for submit', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    const wrapper = shallow(<Form {...props} />, { context })
    const submitButtonProps = wrapper.find('button').props()
    expect(submitButtonProps.type).to.equal('submit')
    expect(submitButtonProps.children).to.equal('Submit')
  })

  it('simulates submit event', () => {
    const props = {
      onSubmit: sinon.spy(),
      location: { pathname: '/signup' },
      isFetching: false,
      error: ''
    }
    const context = { router: {} }
    const wrapper = shallow(<Form {...props} />, { context })
      .setState({ email: 'test@test.com', password: 'test' })
    wrapper.find('form').simulate('submit', { preventDefault() {} })
    expect(props.onSubmit.calledOnce).to.be.true
  })

})