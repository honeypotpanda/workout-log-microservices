import React from 'react'
import { shallow, mount } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'
import { List } from 'immutable'

import { DateList } from '../../src/containers/DateList'

describe('<DateList />', () => {

  it('renders empty div if there are no dates', () => {
    const props = {
      activeDate: '2016-01-01',
      dates: List.of(),
      onDateClick: sinon.spy(),
      fetchDates: sinon.spy()
    }
    const wrapper = shallow(<DateList {...props} />)
    const emptyDivProps = wrapper.find('div').props()
    expect(emptyDivProps).to.deep.equal({})
  })

  it('renders <h2>', () => {
    const props = {
      activeDate: '2016-01-01',
      dates: List.of('2016-01-02', '2016-01-03'),
      onDateClick: sinon.spy(),
      fetchDates: sinon.spy()
    }
    const wrapper = shallow(<DateList {...props} />)
    const headerProps = wrapper.find('h2').props()
    expect(headerProps.children).to.equal('Dates')
  })

  it('renders <ul>', () => {
    const props = {
      activeDate: '2016-01-01',
      dates: List.of('2016-01-02', '2016-01-03'),
      onDateClick: sinon.spy(),
      fetchDates: sinon.spy()
    }
    const wrapper = shallow(<DateList {...props} />)
    expect(wrapper.find('ul').length).to.equal(1)
  })

  it('renders two <Date />', () => {
    const props = {
      activeDate: '2016-01-01',
      dates: List.of('2016-01-02', '2016-01-03'),
      onDateClick: sinon.spy(),
      fetchDates: sinon.spy()
    }
    const wrapper = shallow(<DateList {...props} />)
    expect(wrapper.find('Date')).to.have.length(2)
    const firstDateProps = wrapper.find('Date').at(0).props()
    expect(firstDateProps.date).to.equal('2016-01-02')
    expect(firstDateProps.onDateClick).to.equal(props.onDateClick)
  })

  it('renders one <ActiveDate /> and one <Date />', () => {
    const props = {
      activeDate: '2016-01-01',
      dates: List.of('2016-01-01', '2016-01-02'),
      onDateClick: sinon.spy(),
      fetchDates: sinon.spy()
    }
    const wrapper = shallow(<DateList {...props} />)
    const activeDate = wrapper.find('ActiveDate')
    expect(activeDate).to.have.length(1)
    expect(wrapper.find('Date')).to.have.length(1)
    expect(activeDate.prop('date')).to.equal('2016-01-01')
  })

  it('calls componentDidMount', () => {
    const props = {
      activeDate: '2016-01-01',
      dates: List(),
      onDateClick: sinon.spy(),
      fetchDates: sinon.spy()
    }
    sinon.spy(DateList.prototype, 'componentDidMount')
    const wrapper = mount(<DateList {...props} />)
    expect(DateList.prototype.componentDidMount.calledOnce).to.be.true
    expect(props.fetchDates.calledOnce).to.be.true
  })

})