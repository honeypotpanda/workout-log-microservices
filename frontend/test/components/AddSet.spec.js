import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import sinon from 'sinon'

import { AddSet } from '../../src/containers/AddSet'

describe('<AddSet />', () => {

  it('renders <form>', () => {
    const props = {
      postSet: sinon.spy(),
      activeDate: '2016-01-01'
    }
    const wrapper = shallow(<AddSet {...props} />)
    const formProps = wrapper.find('form').props()
    expect(formProps.onSubmit).to.exist
  })

  it('renders <input> for exercise', () => {
    const props = {
      postSet: sinon.spy(),
      activeDate: '2016-01-01'
    }
    const wrapper = shallow(<AddSet {...props} />)
    const exerciseInputProps = wrapper.find('input').at(0).props()
    expect(exerciseInputProps.type).to.equal('text')
    expect(exerciseInputProps.placeholder).to.equal('Exercise')
    expect(exerciseInputProps.required).to.be.true
    expect(exerciseInputProps.onChange).to.exist
  })

  it('renders <input> for weight', () => {
    const props = {
      postSet: sinon.spy(),
      activeDate: '2016-01-01'
    }
    const wrapper = shallow(<AddSet {...props} />)
    const weightInputProps = wrapper.find('input').at(1).props()
    expect(weightInputProps.type).to.equal('number')
    expect(weightInputProps.placeholder).to.equal('Weight')
    expect(weightInputProps.min).to.equal('0')
    expect(weightInputProps.required).to.be.true
    expect(weightInputProps.onChange).to.exist
  })

  it('renders <input> for repetitions', () => {
    const props = {
      postSet: sinon.spy(),
      activeDate: '2016-01-01'
    }
    const wrapper = shallow(<AddSet {...props} />)
    const repetitionsInputProps = wrapper.find('input').at(2).props()
    expect(repetitionsInputProps.type).to.equal('number')
    expect(repetitionsInputProps.placeholder).to.equal('Repetitions')
    expect(repetitionsInputProps.min).to.equal('1')
    expect(repetitionsInputProps.required).to.be.true
    expect(repetitionsInputProps.onChange).to.exist
  })

  it('renders <button>', () => {
    const props = {
      postSet: sinon.spy(),
      activeDate: '2016-01-01',
    }
    const wrapper = shallow(<AddSet {...props} />)
    const buttonProps = wrapper.find('button').props()
    expect(buttonProps.type).to.equal('submit')
    expect(buttonProps.children).to.equal('Add Set')
  })

  it('simulates submit event', () => {
    const props = {
      postSet: sinon.spy(),
      activeDate: '2016-01-01'
    }
    const state = {
      exercise: 'Squat',
      weight: 135,
      repetitions: 5
    }
    const wrapper = shallow(<AddSet {...props} />)
      .setState(state)
    const form = wrapper.find('form')
    form.simulate('submit', { preventDefault() {} })
    expect(props.postSet.calledOnce).to.be.true
    expect(props.postSet.calledWithExactly('2016-01-01', 'Squat', 135, 5)).to.be.true
  })

})