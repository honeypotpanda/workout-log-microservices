'use strict';

require('dotenv').config();
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const moment = require('moment');
const sequelize = require('./sequelize');
const Workout = require('./workout');
const app = express();
const isValidToken = expressJwt({ secret: process.env.JWT_SECRET });

let port;
if (process.env.NODE_ENV === 'production') {
  port = process.env.PORT;
} else if (process.env.NODE_ENV === 'development') {
  port = 4001;
} else {
  port = 4002;
}

if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
  Workout
    .sync()
    .catch(console.error);
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  origin: ['http://localhost:5000'],
  credentials: true
}));

app.get('/api/workouts/:year-:month-:day', isValidToken, (req, res) => {
  const userId = req.user.sub;
  const date = `${req.params.year}-${req.params.month}-${req.params.day}`;

  if (!moment(date, 'YYYY-MM-DD', true).isValid()) {
    return res.status(400).send({ message: 'Date must be YYYY-MM-DD.' });
  }

  Workout
    .findAll({
      attributes: { exclude: ['user_id'] },
      where: {
        user_id: userId,
        date: date
      }
    })
    .then(workoutSets => {
      res.status(200).send(workoutSets.map(workoutSet => {
        return workoutSet.info;
      }));
    })
    .catch(err => {
      res.status(409).send(err.errors[0]);
    });
});

app.post('/api/workouts', isValidToken, (req, res) => {
  const userId = req.user.sub;
  const exercise = req.body.exercise;
  const weight = req.body.weight;
  const repetitions = req.body.repetitions;
  const date = req.body.date;

  if (!exercise) {
    return res.status(400).send({ message: 'Exercise is required.' });
  }

  if (!(weight >= 0)) {
    return res.status(400).send({ message: 'Weight must be greater than or equal to 0.' });
  }

  if (!(repetitions >= 1)) {
    return res.status(400).send({ message: 'Repetitions must be greater than or equal to 1.' });
  }

  if (!moment(date, 'YYYY-MM-DD', true).isValid()) {
    return res.status(400).send({ message: 'Date must be YYYY-MM-DD.' });
  }

  Workout
    .create({
      user_id: userId,
      exercise: exercise,
      weight: weight,
      repetitions: repetitions,
      date: date
    })
    .then(workout => {
      res.status(201).send(workout.info);
    })
    .catch(err => {
      res.status(409).send(err.errors[0]);
    });
});

app.get('/api/workouts/dates', isValidToken, (req, res) => {
  const userId = req.user.sub;

  sequelize
    .query('SELECT DISTINCT date FROM workouts WHERE user_id = ?', { replacements: [userId], type: sequelize.QueryTypes.SELECT })
    .then(dates => {
      res.status(200).send(dates);
    })
    .catch(err => {
      res.status(400).send(err);
    });
});

app.delete('/api/workouts/:workoutId', isValidToken, (req, res) => {
  const userId = req.user.sub;
  const workoutId = req.params.workoutId;

  Workout
    .destroy({
      where: {
        id: workoutId,
        user_id: userId
      }
    })
    .then(deletedRows => {
      if (deletedRows === 0) {
        return res.status(404).send({ message: 'Unable to delete workout set.' });
      }
      res.status(200).send({ message: 'Successfully deleted workout set.' });
    })
    .catch(err => {
      res.status(400).send(err);
    });
});

app.put('/api/workouts/:workoutId', isValidToken, (req, res) => {
  const userId = req.user.sub;
  const workoutId = req.params.workoutId;
  const exercise = req.body.exercise;
  const weight = req.body.weight;
  const repetitions = req.body.repetitions;
  const date = req.body.date;

  if (!exercise) {
    return res.status(400).send({ message: 'Exercise is required.' });
  }

  if (!(weight >= 0)) {
    return res.status(400).send({ message: 'Weight must be greater than or equal to 0.' });
  }

  if (!(repetitions >= 1)) {
    return res.status(400).send({ message: 'Repetitions must be greater than or equal to 1.' });
  }

  if (!moment(date, 'YYYY-MM-DD', true).isValid()) {
    return res.status(400).send({ message: 'Date must be YYYY-MM-DD.' });
  }

  const updatedValues = {
    exercise: exercise,
    weight: parseInt(weight),
    repetitions: parseInt(repetitions),
    date: date
  };

  Workout
    .update(updatedValues, {
      where: {
        id: workoutId,
        user_id: userId
      }
    })
    .then(result => {
      const updatedRows = result[0];
      if (updatedRows === 0) {
        return res.status(404).send({ message: 'Unable to update workout set.' });
      }
      const updatedWorkout = Object.assign({}, updatedValues, { id: parseInt(workoutId) });
      res.status(200).send(updatedWorkout);
    })
    .catch(err => {
      res.status(400).send(err);
    });
});

app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
  next();
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(port, () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`Server is listening on port ${port}.`);
  }
});

module.exports = app;