'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const jwt = require('jsonwebtoken');
const server = require('./server');
const Workout = require('./Workout');
const body = {
  exercise: 'Squat',
  weight: 135,
  repetitions: 5,
  date: '2016-01-01'
};
const generateJwt = (id, expiry, secret) => (
  jwt.sign({
    iss: 'workout-log-microservices',
    sub: id
  }, secret, { expiresIn: expiry })
);
const userId = 1;
const validToken = generateJwt(userId, '2h', process.env.JWT_SECRET);
const invalidToken = generateJwt(userId, '2h', 'invalid-secret');
const bearerValidToken = `Bearer ${validToken}`;
const bearerInvalidToken = `Bearer ${invalidToken}`;

chai.use(chaiHttp);

describe('Workout', () => {

  before(done => {
    Workout
      .sync({ force: true })
      .then(() => Workout.create(Object.assign({}, body, { user_id: userId, date: '2016-01-02' })))
      .then(() => Workout.create(Object.assign({}, body, { user_id: userId, date: '2016-01-02' })))
      .then(() => done())
      .catch(done);
  });

  after(done => {
    Workout
      .drop()
      .then(() => done())
      .catch(done);
  });

  describe('GET /api/workouts/:year-:month-:day', () => {

    it('should GET workout sets with a valid token and date', done => {
      chai
        .request(server)
        .get('/api/workouts/2016-01-02')
        .set('Authorization', bearerValidToken)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(200);
          res.body.should.be.an('array').with.length(2);
          res.body[0].should.have.property('id');
          res.body[0].should.have.property('exercise');
          res.body[0].should.have.property('weight');
          res.body[0].should.have.property('repetitions');
          res.body[0].should.have.property('date');
          res.body[0].should.have.property('updatedAt');
          res.body[0].should.have.property('createdAt');
          done();
        });
    });

    it('should not GET workout sets without a token', done => {
      chai
        .request(server)
        .get('/api/workouts/2016-01-02')
        .end((err, res) => {
          res.should.have.status(401);
          res.body.message.should.equal('UnauthorizedError: No authorization token was found');
          done();
        });
    });

    it('should not GET workout sets with an invalid token', done => {
      chai
        .request(server)
        .get('/api/workouts/2016-01-02')
        .set('Authorization', bearerInvalidToken)
        .end((err, res) => {
          res.should.have.status(401);
          res.body.message.should.equal('UnauthorizedError: invalid signature');
          done();
        });
    });

    it('should not GET workout sets with an improper date', done => {
      chai
        .request(server)
        .get('/api/workouts/an-improper-date')
        .set('Authorization', bearerValidToken)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Date must be YYYY-MM-DD.');
          done();
        });
    });

    it('should not GET workout sets with an improper date', done => {
      chai
        .request(server)
        .get('/api/workouts/2016-31-01')
        .set('Authorization', bearerValidToken)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Date must be YYYY-MM-DD.');
          done();
        });
    });

  });

  describe('POST /api/workouts', () => {

    it('should POST a workout set with a valid token and body', done => {
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(body)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(201);
          res.body.should.have.property('id');
          res.body.should.have.property('exercise');
          res.body.should.have.property('weight');
          res.body.should.have.property('repetitions');
          res.body.should.have.property('date');
          res.body.should.have.property('updatedAt');
          res.body.should.have.property('createdAt');
          done();
        });
    });

    it('should not POST a workout set without a token', done => {
      chai
        .request(server)
        .post('/api/workouts')
        .send(body)
        .end((err, res) => {
          res.should.have.status(401);
          res.body.message.should.equal('UnauthorizedError: No authorization token was found');
          done();
        });
    });

    it('should not POST a workout set with an invalid token', done => {
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerInvalidToken)
        .send(body)
        .end((err, res) => {
          res.should.have.status(401);
          res.body.message.should.equal('UnauthorizedError: invalid signature');
          done();
        });
    });

    it('should not POST a workout set without exercise', done => {
      const invalidBody = Object.assign({}, body, { exercise: '' });
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Exercise is required.');
          done();
        });
    });

    it('should not POST a workout set with weight less than 0', done => {
      const invalidBody = Object.assign({}, body, { weight: -1 });
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Weight must be greater than or equal to 0.');
          done();
        });
    });

    it('should not POST a workout set with repetitions less than 1', done => {
      const invalidBody = Object.assign({}, body, { repetitions: 0 });
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Repetitions must be greater than or equal to 1.');
          done();
        });
    });

    it('should not POST a workout set with YYYY-DD-MM', done => {
      const invalidBody = Object.assign({}, body, { date: '2016-31-01' });
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Date must be YYYY-MM-DD.');
          done();
        });
    });

    it('should not POST a workout set with DD-MM-YYYY', done => {
      const invalidBody = Object.assign({}, body, { date: '01-31-2016' });
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Date must be YYYY-MM-DD.');
          done();
        });
    });

    it('should not POST a workout set with YYYY', done => {
      const invalidBody = Object.assign({}, body, { date: '2016' });
      chai
        .request(server)
        .post('/api/workouts')
        .set('Authorization', bearerValidToken)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Date must be YYYY-MM-DD.');
          done();
        });
    });

  });

  describe('GET /api/workouts/dates', () => {

    const newUserId = 2;
    const newValidToken = generateJwt(newUserId, '2h', process.env.JWT_SECRET);
    const newInvalidToken = generateJwt(userId, '2h', 'invalid-secret');
    const newBearerValidToken = `Bearer ${newValidToken}`;
    const newBearerInvalidToken = `Bearer ${newInvalidToken}`;

    it('should get three distinct dates', done => {
      Workout
        .sync()
        .then(() => Workout.create(Object.assign({}, body, { user_id: newUserId, date: '2016-01-01' })))
        .then(() => Workout.create(Object.assign({}, body, { user_id: newUserId, date: '2016-01-01' })))
        .then(() => Workout.create(Object.assign({}, body, { user_id: newUserId, date: '2016-01-02' })))
        .then(() => Workout.create(Object.assign({}, body, { user_id: newUserId, date: '2016-01-03' })))
        .then(() => {
          chai
            .request(server)
            .get('/api/workouts/dates')
            .set('Authorization', newBearerValidToken)
            .end((err, res) => {
              should.not.exist(err);
              res.should.have.status(200);
              res.body.should.be.an('array').with.length(3);
              done();
            });
        })
        .catch(done);
    });

    it('should not get dates with an invalid token', done => {
      chai
        .request(server)
        .get('/api/workouts/dates')
        .set('Authorization', newBearerInvalidToken)
        .end((err, res) => {
          res.should.have.status(401);
          res.body.message.should.equal('UnauthorizedError: invalid signature');
          done();
        });
    });

  });

  describe('DELETE /api/workouts/:workoutId', () => {

    const newUserId = 3;
    const newValidToken = generateJwt(newUserId, '2h', process.env.JWT_SECRET);
    const newInvalidToken = generateJwt(userId, '2h', 'invalid-secret');
    const newBearerValidToken = `Bearer ${newValidToken}`;
    const newBearerInvalidToken = `Bearer ${newInvalidToken}`;

    it('should DELETE a workout set with a valid workout id', done => {
      Workout
        .create(Object.assign({}, body, { user_id: newUserId }))
        .then(workout => {
          chai
            .request(server)
            .delete(`/api/workouts/${workout.id}`)
            .set('Authorization', newBearerValidToken)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.message.should.equal('Successfully deleted workout set.');
              done();
            });
        })
        .catch(done);
    });

    it('should not DELETE a nonexistent workout id', done => {
      chai
        .request(server)
        .delete('/api/workouts/1234')
        .set('Authorization', newBearerValidToken)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('should not DELETE with an invalid token', done => {
      Workout
        .create(Object.assign({}, body, { user_id: newUserId }))
        .then(workout => {
          chai
            .request(server)
            .delete(`/api/workouts/${workout.id}`)
            .set('Authorization', newBearerInvalidToken)
            .end((err, res) => {
              res.should.have.status(401);
              res.body.message.should.equal('UnauthorizedError: invalid signature');
              done();
            });
        })
        .catch(done);
    });

  });

  describe('PUT /api/workouts/:workoutId', () => {

    const newUserId = 4;
    const newValidToken = generateJwt(newUserId, '2h', process.env.JWT_SECRET);
    const newInvalidToken = generateJwt(userId, '2h', 'invalid-secret');
    const newBearerValidToken = `Bearer ${newValidToken}`;
    const newBearerInvalidToken = `Bearer ${newInvalidToken}`;
    const newBody = {
      exercise: 'Deadlift',
      weight: 225,
      repetitions: 10,
      date: '2016-01-02'
    };
    let workoutId = -1;

    before(done => {
      Workout
        .create(Object.assign({}, body, { user_id: newUserId }))
        .then(workout => {
          workoutId = workout.id;
          done();
        })
        .catch(done);
    });

    it('should PUT a workout set with a valid workout id and body', done => {
      chai
        .request(server)
        .put(`/api/workouts/${workoutId}`)
        .send(newBody)
        .set('Authorization', newBearerValidToken)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.id.should.equal(workoutId);
          res.body.exercise.should.equal(newBody.exercise);
          res.body.weight.should.equal(newBody.weight);
          res.body.repetitions.should.equal(newBody.repetitions);
          res.body.date.should.equal(newBody.date);
          done();
        });
    });

    it('should not PUT with an invalid id', done => {
      const invalidId = workoutId + 1;
      chai
        .request(server)
        .put(`/api/workouts/${invalidId}`)
        .send(newBody)
        .set('Authorization', newBearerValidToken)
        .end((err, res) => {
          res.should.have.status(404);
          res.body.message.should.equal('Unable to update workout set.');
          done();
        });
    });

    it('should not PUT without exercise', done => {
      chai
        .request(server)
        .put(`/api/workouts/${workoutId}`)
        .set('Authorization', newBearerValidToken)
        .send(Object.assign({}, body, { exercise: '' }))
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Exercise is required.');
          done();
        });
    });

    it('should not PUT with weight less than 0', done => {
      chai
        .request(server)
        .put(`/api/workouts/${workoutId}`)
        .set('Authorization', newBearerValidToken)
        .send(Object.assign({}, body, { weight: -1 }))
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Weight must be greater than or equal to 0.');
          done();
        });
    });

    it('should not PUT with repetitions less than 1', done => {
      chai
        .request(server)
        .put(`/api/workouts/${workoutId}`)
        .set('Authorization', newBearerValidToken)
        .send(Object.assign({}, body, { repetitions: 0 }))
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Repetitions must be greater than or equal to 1.');
          done();
        });
    });

    it('should not PUT with an improper date', done => {
      chai
        .request(server)
        .put(`/api/workouts/${workoutId}`)
        .set('Authorization', newBearerValidToken)
        .send(Object.assign({}, body, { date: '2016-31-01' }))
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.equal('Date must be YYYY-MM-DD.');
          done();
        });
    });

    it('should not PUT with an invalid token', done => {
      chai
        .request(server)
        .put(`/api/workouts/${workoutId}`)
        .set('Authorization', newBearerInvalidToken)
        .send(newBody)
        .end((err, res) => {
          res.should.have.status(401);
          res.body.message.should.equal('UnauthorizedError: invalid signature');
          done();
        });
    });

  });

});