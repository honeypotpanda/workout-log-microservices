'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./sequelize');
const Workout = sequelize.define('workout', {
  user_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  exercise: {
    type: Sequelize.TEXT('tiny'),
    allowNull: false
  },
  weight: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  repetitions: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  date: {
    type: Sequelize.DATEONLY,
    allowNull: false
  }
}, {
  getterMethods: {
    info: function() {
      return {
        id: this.id,
        exercise: this.exercise,
        weight: this.weight,
        repetitions: this.repetitions,
        date: this.date,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
      };
    }
  }
});

module.exports = Workout;